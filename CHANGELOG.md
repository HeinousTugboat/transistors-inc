# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.1.1...v0.1.2) (2020-08-15)


### Features

* **ai-trainer.service:** overhauls AI generation, adds allocation sliders ([0fc64ab](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0fc64ab75c5108b6d438c18f9c5ab4b5dcaf2c2c)), closes [#60](https://gitlab.com/HeinousTugboat/transistors-inc/issues/60)
* **app.component:** updates top panel to use separate panels, adds breakpoints ([685f6f9](https://gitlab.com/HeinousTugboat/transistors-inc/commit/685f6f9da59532023a32ae8f5713ae5405227eb6)), closes [#57](https://gitlab.com/HeinousTugboat/transistors-inc/issues/57)
* **home.component:** adds header for chip allocation slider-set ([a2eab2a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a2eab2a81790e2159060f079735a43088cf3d2c4))
* **options.service:** adds chip allocation ratios ([fd35032](https://gitlab.com/HeinousTugboat/transistors-inc/commit/fd35032cc011e07ccaf425c486b389e29fdfb1df))
* **random-bag:** adds random bag class and tests ([bdbd62a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/bdbd62ac1e253dcab4447d349d971529554e0400))
* **slider-set.component:** adds multi-slider component and interface ([e39b652](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e39b652aeb67deecaa068ef9ba4d679bb5099118))
* **slider.component:** adds lock, redesigns slider ([54cfce5](https://gitlab.com/HeinousTugboat/transistors-inc/commit/54cfce5d9f36c4474c3ae030214754fdec6cc1c3))
* **slider.component:** adds touch events to sliders ([1b0f2ce](https://gitlab.com/HeinousTugboat/transistors-inc/commit/1b0f2ced5ce96f092ce5eaabd792b88ab034bdb2))
* **styles:** adds global scss variables ([f69a171](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f69a171242ac049293ea1ba9e130bd8af55d9ef5))
* **styles:** updates how font selection works, adds new copy font, adds ability to select font ([b4a255e](https://gitlab.com/HeinousTugboat/transistors-inc/commit/b4a255e5a92a6516d7d0702dd6404c1fc0081e29))
* **transistors.service:** adjusts replication rates for +spd and +r seed states ([678035c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/678035c2a63f54f95ee5ce78378e8defea2dd752))


### Bug Fixes

* **currency.service:** updates subtract allow 0 to be subtracted ([2f9823e](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2f9823eb500ca3abb6e3aa6c975d4d3b76b4fed7))
* **slider.component:** misc fixes around slider and slider-set ([5a6a328](https://gitlab.com/HeinousTugboat/transistors-inc/commit/5a6a3282e1ce03094285274d4d19255dcb5d362b))
* **upgrade.data:** fixes missing descriptions from chip/cycle upgrades ([c0a2d4a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c0a2d4abc4dcb1d8845f0c1ebd7f1254f6111199))

### [0.1.1](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.1.0...v0.1.1) (2020-08-01)


### Features

* **ai:** flips AI rarity to AI promotion, updates UI to reflect ([0d266fd](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0d266fde400f76c743945cbc0200ec427260cf27)), closes [#75](https://gitlab.com/HeinousTugboat/transistors-inc/issues/75)
* **ai.service:** moves rarity bonus to mod service to decouple AI from Upgrades ([90061dc](https://gitlab.com/HeinousTugboat/transistors-inc/commit/90061dc44d368ab31fbd61165acf6da9b26e31c9)), closes [#60](https://gitlab.com/HeinousTugboat/transistors-inc/issues/60) [#65](https://gitlab.com/HeinousTugboat/transistors-inc/issues/65)
* **currency.service:** renames accumulated transistors to chips, changes transistor and chip gain ([d86bc78](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d86bc7844fa29e363e9621fd7d27af964aac8fa7)), closes [#56](https://gitlab.com/HeinousTugboat/transistors-inc/issues/56)
* **help.component:** adds more cleanup and styling to help page ([0b7a074](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0b7a07457fea26d70356e0c4525a94a7c1f0d7fb)), closes [#66](https://gitlab.com/HeinousTugboat/transistors-inc/issues/66) [#58](https://gitlab.com/HeinousTugboat/transistors-inc/issues/58)
* **help.component:** moves glass test functionality to options page, adds actual help data ([cbc1564](https://gitlab.com/HeinousTugboat/transistors-inc/commit/cbc156425745886e77d7dab7a6e17d0f1bd51e18)), closes [#66](https://gitlab.com/HeinousTugboat/transistors-inc/issues/66) [#58](https://gitlab.com/HeinousTugboat/transistors-inc/issues/58)
* **help.component:** removes font to appease the haters ([c713bda](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c713bdad280aaf74dd589e6b48d5335a96168b3e))
* **keybind.service:** updates letter existing keybinds to also honor uppercase keys ([83f5674](https://gitlab.com/HeinousTugboat/transistors-inc/commit/83f5674236b395a7c5eac60bd1e671cbc4f016f4)), closes [#69](https://gitlab.com/HeinousTugboat/transistors-inc/issues/69)
* **logbook.component:** moves log from home page to dedicated logbook page ([6056fb1](https://gitlab.com/HeinousTugboat/transistors-inc/commit/6056fb10233f9acd7218548e730bcd7d1485ec16)), closes [#63](https://gitlab.com/HeinousTugboat/transistors-inc/issues/63)
* **social-media.component:** adds social media floater ([af8007a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/af8007af6605bcb46c8476a1490081665853a54b)), closes [#59](https://gitlab.com/HeinousTugboat/transistors-inc/issues/59)
* **styles:** adds PT Mono font ([5298637](https://gitlab.com/HeinousTugboat/transistors-inc/commit/5298637d3da5f30280630de20b210b1265857091))
* **tick.service:** adds 100x multipllier to cap growth ([b6fae09](https://gitlab.com/HeinousTugboat/transistors-inc/commit/b6fae09d2d49c089f9d18d6269f16695da11d9e6)), closes [#64](https://gitlab.com/HeinousTugboat/transistors-inc/issues/64)
* **top-pane.component:** updates chip total formatting, adds round chip total ([68e521f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/68e521f00611cca57e55f01ccc8e01a6ce715641)), closes [#77](https://gitlab.com/HeinousTugboat/transistors-inc/issues/77)
* **upgrade.data:** update round length and autobuyer upgrade and mod data to make more sense ([9dcb8b6](https://gitlab.com/HeinousTugboat/transistors-inc/commit/9dcb8b628ede08a8e3c965ff6551a11eefc3da58)), closes [#72](https://gitlab.com/HeinousTugboat/transistors-inc/issues/72) [#70](https://gitlab.com/HeinousTugboat/transistors-inc/issues/70)
* renames compute to cycles ([9cfdf85](https://gitlab.com/HeinousTugboat/transistors-inc/commit/9cfdf853b6dabc17ff2a293df8f8678d06bb3286)), closes [#74](https://gitlab.com/HeinousTugboat/transistors-inc/issues/74) [#73](https://gitlab.com/HeinousTugboat/transistors-inc/issues/73)


### Bug Fixes

* **ai-list.component:** fixes typo for Delverton ([4198972](https://gitlab.com/HeinousTugboat/transistors-inc/commit/41989722a557434c0a9469d03b003ac6be1bc307))
* **help.component:** updates the credits section to be clearer ([0f182e6](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0f182e6b12939e235ccfe35d140eda0c49bdd4ed))
* **mod.service:** adds chip mods to recalculation ([5b97ce1](https://gitlab.com/HeinousTugboat/transistors-inc/commit/5b97ce157b02784bb1359a0f4ff86d5c2f859ad3)), closes [#62](https://gitlab.com/HeinousTugboat/transistors-inc/issues/62)
* **top-pane.component:** another swing at the chip display ([3a79c06](https://gitlab.com/HeinousTugboat/transistors-inc/commit/3a79c069b4821c5177944fdf47aae08d5524896a))
* **upgrades.service:** fixes miscalculated ai bonus ([0259d5a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0259d5a3f9a4b6f03b60f57733e36db781052339))

## [0.1.0](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.1.0-alpha.10...v0.1.0) (2020-07-27)


### Features

* **ai:** removes mass mod ([c656dbd](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c656dbd179732e0805d0b2577e8af74c637c7ac9)), closes [#47](https://gitlab.com/HeinousTugboat/transistors-inc/issues/47)
* **augments:** removes augments ([0e6010c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0e6010c5162a06165e8a4f71d52a7a53bb78fbfd))
* **augments.service:** adds first pass at implementing augments ([492cff2](https://gitlab.com/HeinousTugboat/transistors-inc/commit/492cff29bf517412014380d0bc0f15df833c338e)), closes [#43](https://gitlab.com/HeinousTugboat/transistors-inc/issues/43)
* **bonus-ticks.service:** updates bonus ticks to properly work from correct service ([c31c5bc](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c31c5bc3d380f5603bd0aaebd2e9f327236980a7))
* **bonus.service:** adds bonus service, updates upgrades to use it ([a73f16a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a73f16a0d0987363faa65ac9fdd1c4c11ab0b0c7))
* **bonus.service:** continues refactor: moves AI modifiers to bonus service ([a164ccd](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a164ccdf433dcdd26319b2c087c9bfa630d75964))
* **environment:** changes dev environment to use distinct storageKey ([f9b5675](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f9b56754b8df0e9594a42f9a7cc4414202674ab7))
* **help.component:** adds buttons and toggle for individual glass labels ([c496e85](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c496e85518a0c4f101defadb1070f494e808224e))
* **help.component:** adds buttons to test various glass labels ([f7695c5](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f7695c55b208b564075b7a7a5870f1ca1f0aba8f))
* **log.service:** adds serialization for log service ([73c27fc](https://gitlab.com/HeinousTugboat/transistors-inc/commit/73c27fc6532b9506c432f06d769e73b38749e390))
* **mod.service:** splits upgrade mods into compute and credit mods, cleans up more stuff ([b20b826](https://gitlab.com/HeinousTugboat/transistors-inc/commit/b20b826977f6f56e11f8ab311e602020e8fc8b2f))
* **nav.component:** moves nav bar to top and updates styling ([a76d5a2](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a76d5a21cdfca9c22021394461ed852037a9b1b9))
* **pages/help.component:** adds help page stub, removes design page ([8786307](https://gitlab.com/HeinousTugboat/transistors-inc/commit/8786307f66a3a3104b7152107704d5195aa34935)), closes [#54](https://gitlab.com/HeinousTugboat/transistors-inc/issues/54)
* **serialization:** udpates serializable to use slices instead of partials ([6859ba0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/6859ba05bd379544c162958a0abe8707ecf019cf))
* **styles.scss:** removes ability to select text globally ([2956215](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2956215c08ad124a1a366292afe7cb8783c297e6))
* **tick.service:** adds AI compute mod to compute generation ([acd65b0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/acd65b0e06c66b7eb3f3bd583df24c83c513e084))
* **tick.service:** updates tick service and transistors service to use new math ([ee3facc](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ee3facc89d743f447dfacafcfa45869c705ba4d0)), closes [#41](https://gitlab.com/HeinousTugboat/transistors-inc/issues/41)
* **transistors.service:** changes canSell to only check currency, not round number ([b284f56](https://gitlab.com/HeinousTugboat/transistors-inc/commit/b284f56ddfa417810a291329867ebb0c66ebc89f))
* **transistors.service:** moves compute upgrade keybinds from upgrades to transistors ([83966b3](https://gitlab.com/HeinousTugboat/transistors-inc/commit/83966b3b94d26c82e071992c3b4861bfcbaacada)), closes [#49](https://gitlab.com/HeinousTugboat/transistors-inc/issues/49)
* **tutorial.service:** adds glass directive and tutorial service ([fab2f67](https://gitlab.com/HeinousTugboat/transistors-inc/commit/fab2f67212247df5f5a8dfb6ff52b8e409271b6e)), closes [#53](https://gitlab.com/HeinousTugboat/transistors-inc/issues/53)
* **units.pipe:** adds cutover to scientific notation at 1e10 ([268b6ce](https://gitlab.com/HeinousTugboat/transistors-inc/commit/268b6ce5ad218f06b14ed3630db51985e6dedbad)), closes [#48](https://gitlab.com/HeinousTugboat/transistors-inc/issues/48)
* **upgrades.component:** updates upgrades page with upgrade-card component ([1a03ccf](https://gitlab.com/HeinousTugboat/transistors-inc/commit/1a03ccf603ccce4d8a169ede53d5fd5a9e75966f)), closes [#50](https://gitlab.com/HeinousTugboat/transistors-inc/issues/50)
* beginning of math rewrite, getting my formulae and invariants in place ([cf2bee4](https://gitlab.com/HeinousTugboat/transistors-inc/commit/cf2bee4199f6aad4ba6cadae99bf2219d4e9a262))
* **upgrades.service:** adds autobuyer delay upgrade ([e685097](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e685097be9efd1745877a3682e2cabad394bb2fd)), closes [#27](https://gitlab.com/HeinousTugboat/transistors-inc/issues/27)
* **upgrades.service:** adds deserialization path for compute upgrades ([f7c0595](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f7c05951b223198550164a1dafb4acfc17faef67))


### Bug Fixes

* **ai.data:** fixes AI mods to be correct ([e926771](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e9267711cc2ae11d6eb65d10847470769607b604))
* **options.service:** corrects notation cutoff as 1e10 instead of just.. 10 ([5f301d8](https://gitlab.com/HeinousTugboat/transistors-inc/commit/5f301d860bb0f73253884db74d90f609ec0b6697))
* **tick.service:** adds AI/elevate compute mod ([e995f33](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e995f33e66f34f16a734fa7139c447ab79131c87))
* adds error handling around deserialization ([3705fd8](https://gitlab.com/HeinousTugboat/transistors-inc/commit/3705fd88d1c1262fca4331554a250a68d88e4f6a))
* **ai.service:** fix eye wateringly strong augment bonus to be less eye watering ([99e6950](https://gitlab.com/HeinousTugboat/transistors-inc/commit/99e69503f9e54ef2bdc8ff09125f561a539521c5))
* **ai.service:** fixes less eye wateringly strong augment bonus to be kinda boringly eye watering ([3b30f88](https://gitlab.com/HeinousTugboat/transistors-inc/commit/3b30f885a74ea5135fbf7b535a98c8beb4d63d09))
* **ai.service:** updates how modifiers are calculated both at deserialization and reset ([7a00257](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7a002576606f8aeb3f8e03636e61f3a7af7a8b87))
* **top-pane.component:** adds imperative blurs to button clicks to keep keybinds flowing ([690cf41](https://gitlab.com/HeinousTugboat/transistors-inc/commit/690cf416eef0ddca0536cbe512e3aa56e89884e2))
* **transistors.service:** removes extra roundNumber check from transistors sell flow ([a217efb](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a217efb59acc26951fccd47759a39cb1cdf1cf66))
* **upgrades.service:** correctly account for purchased upgrades when deserializing ([f63ffd0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f63ffd00736ecdff336e3341b7c49708736519e1))

## [0.1.0-alpha.10](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.9...v0.1.0-alpha.10) (2020-07-13)


### ⚠ BREAKING CHANGES

* **serializationservice:** Previous versions of local storage state will not transfer because I've made the
"gameVersion" key required

### Features

* **ai:** adds serialization for ais ([fff4a66](https://gitlab.com/HeinousTugboat/transistors-inc/commit/fff4a6675fdbbc239f2ad3c7d8a63be60a27fb02))
* converts round length upgrade to come from upgrades service ([b7a30e0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/b7a30e04045b368c93f76f8bf6d9ea083be6163b))
* **augment-list.component:** adds augment list placeholder ([a613978](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a613978e803dccac1567b3547536206341996945))
* **augment-list.component:** adds basic animation stuff ([15683a5](https://gitlab.com/HeinousTugboat/transistors-inc/commit/15683a598a5f2da6d1c69e9e18c672275fd9a2fa))
* **augment-list.component:** adds fancier lines, and a Q curve. Q! ([36c0a0f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/36c0a0fd4cde2420cd79bbbd1c0e3dfd621efeb2))
* **options:** moves debug flag to options service ([41817bf](https://gitlab.com/HeinousTugboat/transistors-inc/commit/41817bfc05224669654a931bd379803de5656d25))
* **serialization:** adds more state stuff to serialization and game state ([4f16b09](https://gitlab.com/HeinousTugboat/transistors-inc/commit/4f16b09ec61ef0a87dacff4bf7ca9cd32803dd77))
* **serialization:** moves initial state to single place, adds random and options to serialization ([1bfe9d2](https://gitlab.com/HeinousTugboat/transistors-inc/commit/1bfe9d25290b1dd3312bade13dda506d31e86f8b))
* **serialization:** updates serialization and friends to use strings ahead of Decimal parsing ([d6ccc24](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d6ccc24a6cbec1e4ee811782010e0067aabd1c90))
* makes Upgrade and Currency Services serializable ([cb961be](https://gitlab.com/HeinousTugboat/transistors-inc/commit/cb961be209fa9bc8966045f34c2370fe1750c7af))
* **serializationservice:** adds SerializableService and starts saving on an interval ([02a9592](https://gitlab.com/HeinousTugboat/transistors-inc/commit/02a9592ff69da761976c1c85ad4a1ab91ab040d4))
* **tslint:** adds function return typedef and fixes it in a lot of places ([1927c18](https://gitlab.com/HeinousTugboat/transistors-inc/commit/1927c1839e164eef85a7d092438725dd4a17e848))
* **utils:** adds defined and and invalid utilities ([06043cd](https://gitlab.com/HeinousTugboat/transistors-inc/commit/06043cd103fc74f2e280549fe67ed62350bd5e55))
* **utils:** adds lerp and tests ([0e00312](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0e00312ee1315a0c367c1bcb705ff2eb03421806))
* **utils:** updates clamp to case min/max to infinity, adds tests ([19e014f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/19e014f388a2038e366d850935a95b205f76dda8))
* **vendor:** adds easing function list from easings.net ([4f5087f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/4f5087f7dfe69d6e9339e3e171df3dbb545e5f26))


### Bug Fixes

* **currency.service:** fixes incorrect deserialization for bonus ticks ([7b88bf0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7b88bf0ce1fc5a0098815c7c0b756b13219d5600))
* **keybind.service:** adds onDestroy to clean up after itself ([a952b84](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a952b84321fc893c6a94a45de3571572ecf9d68b))

### [0.0.1-alpha.9](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.8...v0.0.1-alpha.9) (2020-06-09)


### Features

* **ai-list.component:** adds button to murder grads ([cb67507](https://gitlab.com/HeinousTugboat/transistors-inc/commit/cb6750798d479b79306c59a680a66de8ecc58cce))
* **ais.component:** adds button to murder undergrads ([8f9c76e](https://gitlab.com/HeinousTugboat/transistors-inc/commit/8f9c76e7655746d6c6583ca3dc280870be32682d))
* **app.component:** removes now dead code in favor of new components ([33845c0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/33845c0f4ab4c1d2b8f5be8aa5f3490019da7f58))
* **balance:** adjusts r multiplier from +r state to 2.0 ([25404bd](https://gitlab.com/HeinousTugboat/transistors-inc/commit/25404bd9b256b838fd2eb46ebcb180fd64a86eb2))
* **bonus-tick.service:** adds initial pass at bonus tick implementation ([e591351](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e5913517efbfd1ed0dd37068d8f24e220e1d2faf))
* **cheat.guard:** hides diagnostics and cheat buttons behind cheat option ([47f30be](https://gitlab.com/HeinousTugboat/transistors-inc/commit/47f30be02ddfacadc46002281c76882ce3f5cd24))
* **commitizen:** adds NPM script for commit ([f646fa3](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f646fa348ff1a1898e3bec7cde2a9539547ccc32))
* **diagnostics.component:** adds number of ai to diagnostics ([7f0ecaa](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7f0ecaa65c2d2f1d3795f464847d3b34720a1f4c))
* **environment:** adds ENV injector and environment interface ([67ca490](https://gitlab.com/HeinousTugboat/transistors-inc/commit/67ca490b8548a65ea18c7482632cf34531d6161f))
* **keybind.service:** adds first pass at keybind service ([5b96cf4](https://gitlab.com/HeinousTugboat/transistors-inc/commit/5b96cf4f7a36d49b7a731b87e9f50dcd3804d376)), closes [#28](https://gitlab.com/HeinousTugboat/transistors-inc/issues/28)
* **options.service:** adds initial cheat guard, updates options screen to, you know, show options ([9587ba6](https://gitlab.com/HeinousTugboat/transistors-inc/commit/9587ba68d4d115a875e1ba2199fc3675f0b8fa7d))
* **scss:** adds global large and spacer styles ([fd31d50](https://gitlab.com/HeinousTugboat/transistors-inc/commit/fd31d50b0be3b67f891e09e41983439c7272327c))
* **top-pane.component:** adjusts top pane layout and UI to be clearer ([fc81f67](https://gitlab.com/HeinousTugboat/transistors-inc/commit/fc81f67ac472e4654f5f7595967e173de50e2e2a)), closes [#42](https://gitlab.com/HeinousTugboat/transistors-inc/issues/42)
* **transistors.service:** updates credit calc so AI bonus is elevated, not whole total ([c87ee52](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c87ee5212d3707cd9615f144a182b5ce08022027)), closes [#44](https://gitlab.com/HeinousTugboat/transistors-inc/issues/44)
* adds nav.component and top-pane.component ([4c2075d](https://gitlab.com/HeinousTugboat/transistors-inc/commit/4c2075d58fd0fc5742846d5466a232a1f8474fc2))
* updates sell buttons, fixes [#14](https://gitlab.com/HeinousTugboat/transistors-inc/issues/14) ([c9644a4](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c9644a4fecd7c44b018dbef9bb8338c0de79f65a))
* **transistors.service:** adds ai mod back to compute generation, hard caps compute to m/sec ([8cbc833](https://gitlab.com/HeinousTugboat/transistors-inc/commit/8cbc833c1f0a1d14a11079636dec09e26a5a75f1))


### Bug Fixes

* **ai.service:** apparently log10 returns a number ([86a3f4a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/86a3f4a53e38332a7a7196be22fa0b4a2740a1ca))
* **ai.service:** changes ai transistor scaling to multiply ([63fb8fa](https://gitlab.com/HeinousTugboat/transistors-inc/commit/63fb8faa185e34bed78aa814671fd3a671225664))
* **ais.component:** removes extra line, adds extra line ([80eda5a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/80eda5ac211a2b594145867dc7de81b822440f6a))
* **ais.component:** updates ai IDs to be More Random, adds make a hundred AIs ([2ed7648](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2ed764854205c8a6dea8fc4d4e31e0a12e4283d0)), closes [#38](https://gitlab.com/HeinousTugboat/transistors-inc/issues/38)
* **pages:** adds missing FontAwesome import ([6fa2f75](https://gitlab.com/HeinousTugboat/transistors-inc/commit/6fa2f75881a55d785897349efb40f5b0ef212887))
* **transistors.service:** fixes bonus tick rate to actually be 100x ([67370ae](https://gitlab.com/HeinousTugboat/transistors-inc/commit/67370aea17edc5eba0ee14a60de6de8f86b61b8e)), closes [#39](https://gitlab.com/HeinousTugboat/transistors-inc/issues/39)
* **units.pipe:** fixes tests resulting from options changes ([343df59](https://gitlab.com/HeinousTugboat/transistors-inc/commit/343df59997cb0309ece85ba7372421c52097ff73))

### [0.0.1-alpha.8](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.7...v0.0.1-alpha.8) (2020-06-04)


### Features

* **ai.service:** changes f to lower per upgrade instead of every 10, calculation is the same ([bd964ae](https://gitlab.com/HeinousTugboat/transistors-inc/commit/bd964ae4dd01057ba0d17618e3677560e4e38536))
* **ai.service:** multiplies line mod by tier + 1 ([13b7084](https://gitlab.com/HeinousTugboat/transistors-inc/commit/13b7084790ddc2c9867485f3b3834ba17a3d7ac8)), closes [#32](https://gitlab.com/HeinousTugboat/transistors-inc/issues/32)
* **ais.component:** updates ai list to use CSS grid and look purty ([2ada5b8](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2ada5b805896e77e2aa10a5caa731b7af63deb45))
* **romu-random:** print out seed when Mono32 fails, adds check for blacklisted seeds ([80d8f34](https://gitlab.com/HeinousTugboat/transistors-inc/commit/80d8f340e7f32ecabd8009548055b9d9d1e04db5))


### Bug Fixes

* **ai.service:** breaks dep cycle between ai.service and transistors.service ([af5eef7](https://gitlab.com/HeinousTugboat/transistors-inc/commit/af5eef7d06bc8b3711385730097f8cd4ac2cfeb9))
* **ai.service:** tier mult should only apply to normal lines ([5cc4ece](https://gitlab.com/HeinousTugboat/transistors-inc/commit/5cc4ece0a0ddf54b1acd244838ecf4697a600508))

### [0.0.1-alpha.7](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.6...v0.0.1-alpha.7) (2020-06-02)


### Features

* **ai.component:** adds initial cards for ai components ([851ab66](https://gitlab.com/HeinousTugboat/transistors-inc/commit/851ab66b30dce6a78606ba96206fabf5579fcc35))
* **ai.service:** adds ability to progress AI generation via the power of the mouse ([126d4fd](https://gitlab.com/HeinousTugboat/transistors-inc/commit/126d4fd3fef8e472940bd711219f8e5da4c8d455)), closes [#30](https://gitlab.com/HeinousTugboat/transistors-inc/issues/30)
* **ais.component:** adds initial ai.component ([64040ae](https://gitlab.com/HeinousTugboat/transistors-inc/commit/64040ae5376c9f7fffc5b1eec257f487b9b1aff7))
* **log:** adds error log entries ([f97d505](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f97d50564c7e8e1da45beadedb625da3986f1384))
* **upgrades.service:** overhauls upgrades.component, adds compute upgrade buttons ([0da67d0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0da67d0d81753fddaca9d078bf499b71cf184c95))
* moves reset button from diagnostics to options ([96ce17e](https://gitlab.com/HeinousTugboat/transistors-inc/commit/96ce17e53843a5148878a63e57938f457cc8a68e))
* updates state buttons to grow to fill width of column ([7666b53](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7666b534b5318f3590ebbec56bf92d393dd9f906))


### Bug Fixes

* **upgrades.data:** corrects generators for m and t compute upgrades ([1e39168](https://gitlab.com/HeinousTugboat/transistors-inc/commit/1e39168229b19051facc47f1e65f3d061f084418))
* **upgrades.service:** updates how compute upgrades get reset to be more correct ([709e3f4](https://gitlab.com/HeinousTugboat/transistors-inc/commit/709e3f44ef593fa3f7cffa205d12d953a370b072))

### [0.0.1-alpha.6](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.5...v0.0.1-alpha.6) (2020-06-01)


### Features

* **diagnostics.component:** adds a big red button ([1ae8556](https://gitlab.com/HeinousTugboat/transistors-inc/commit/1ae855656b348f90f22aace5df4f80deed774599))
* adds initial slider component and options page ([7cda978](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7cda97856db194303577f67360af9059940b95b9))
* adds new sell buttons ([2cb7365](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2cb736504c58ba7f143abed3724d0c99f47548da))
* adds progress bar for AI generation ([d795dab](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d795dab6419771dd081d43adaf2ef74e2d5f8c0c)), closes [#15](https://gitlab.com/HeinousTugboat/transistors-inc/issues/15)
* **ai.service:** adds f calculation ([59a893a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/59a893a8320e1e34a230ce8222749dcabe40b03b))
* **ai.service:** rounds out upgrades and modifiers for AI and Upgrades ([61a06c1](https://gitlab.com/HeinousTugboat/transistors-inc/commit/61a06c17a6ebd29f2f618e703f831283cbcd4852))
* **ai.service:** updates so all AI tiers get 20% chance of special line 3 ([f6b5d3a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f6b5d3add3511969b6ea6bad9cc72fcae4310357))
* **transistors.service:** incorporates AI mod updates, rewrites Compute equation .. again ([cd87ed1](https://gitlab.com/HeinousTugboat/transistors-inc/commit/cd87ed1e04473c698c61bf240b192f40c12ca2e9))
* **upgrades.service:** add compute upgrades, updates credit upgrades ([e942f14](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e942f1493d40b68ab952e69d0fc5f62df0d1c69f)), closes [#8](https://gitlab.com/HeinousTugboat/transistors-inc/issues/8)
* **upgrades.service:** adds dynamic ids to upgrades, rehydrates using dynamic ids ([d18f9de](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d18f9deb8f339405f2bdd9e189356b7e458ffb5f))


### Bug Fixes

* **ai.service:** corrects nobel AI calculating lines incorrectly ([645b2dc](https://gitlab.com/HeinousTugboat/transistors-inc/commit/645b2dc59ad24c1851f9981f8e9ada1b5aac25f2))
* **log.component:** changes history to slice from end instead of start ([d0d205a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d0d205aa873b86a0f61bb0e65d23240e87e37af3))
* **transistors.service:** updates sell code to properly sell accumulated transistors ([a7972d5](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a7972d59b063322bed87946cf8982960f28e46b7))
* **upgrades.service:** adds recalc to resetCompute.. oops ([5d403a2](https://gitlab.com/HeinousTugboat/transistors-inc/commit/5d403a2fb589c3af04b890f1dc97e6d2063d1df3))
* **upgrades.service:** removes purchased upgrades from available upgrades on rehydration ([90edd32](https://gitlab.com/HeinousTugboat/transistors-inc/commit/90edd32c610ddd2a732ed8ffbfadffb78f732cae))

### [0.0.1-alpha.5](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.3...v0.0.1-alpha.5) (2020-05-30)


### ⚠ BREAKING CHANGES

* **ai.service:** AI has a different interface now
* numbers are numbers no longer

### Features

* **ai.service:** completely overhauls AI system and classes ([fea712c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/fea712cd8797d6551f1e9b7f6d0f010bdf28103e)), closes [#16](https://gitlab.com/HeinousTugboat/transistors-inc/issues/16)
* **app.component:** refactors round timer to use new progress bar component ([e0ffd8f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e0ffd8fdcde0f37358d2e6513db91a66107316d5))
* **currency.service:** adds currency service and existing currencies (and tests! wow!) ([d72d9e7](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d72d9e718434dcf9d1bed95f35c0c5caa92bce98))
* **currency.service:** updates all components/services to utilize currency service ([8d42eb2](https://gitlab.com/HeinousTugboat/transistors-inc/commit/8d42eb2ddb7f3383d0ad38de0478a1d01a3a0da7)), closes [#12](https://gitlab.com/HeinousTugboat/transistors-inc/issues/12) [#3](https://gitlab.com/HeinousTugboat/transistors-inc/issues/3)
* **diagnostics.component:** adds button to add 10k accumulated transistors ([cff1a40](https://gitlab.com/HeinousTugboat/transistors-inc/commit/cff1a400e325126524a88da74227f0b4c7819339))
* **diagnostics.component:** splits diagnostics into separate floating component ([13d68bb](https://gitlab.com/HeinousTugboat/transistors-inc/commit/13d68bb574679505880af64d874e0351c822e8ad))
* **log.component:** reverses event log order, fades items so only 15 most recent are visible ([c31598c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c31598c26bd434a02624b973560d1140dcce72d5))
* **log.components:** adds logLength to options service, takes slice of log instead of full thing ([aeeda2c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/aeeda2ca52d4e8c20f3ae1dd86888c166e573430))
* **progress.component:** adds progress component to show dynamic width bars ([ad37912](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ad379126919c4b5ee97c5eacde9bdca719fe0206))
* **random.service:** adds initial pass and RNG service, adds diagnostics to check results ([b37dda6](https://gitlab.com/HeinousTugboat/transistors-inc/commit/b37dda66c1cd0febfb99a211c4d533c47a27decc))
* **romu-random:** adds fastrange to DuoJr ([eb6cc40](https://gitlab.com/HeinousTugboat/transistors-inc/commit/eb6cc40406ba62de04ddf0c83d5966eab826f9f5))
* **romu-random:** adds init function to DuoJr ([a7de27f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a7de27f20b18c9ea37216f1b09ddf16da799728f))
* **romu-random:** adds RNG type for the one we're actually using ([ed568ec](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ed568ecccb4c016e6a66f4f81e4381da73189c78))
* **romu-random:** updates PRNG interfaces to use better names ([a792c02](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a792c022a2490bdacfd5b6ea178a641b18940af0))
* adds romu-random algorithms and tests, adds BigInt support ([a9fccf4](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a9fccf411295583122cc69175bb302a79b706332))
* enables strict mode, updates code to follow strict mode restrictions ([497c2a6](https://gitlab.com/HeinousTugboat/transistors-inc/commit/497c2a642617c0eba04efcc7883798d165a4105d))
* replaces number with break_infinity's Decimal ([d5eb42e](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d5eb42e131b24284138c7c64281dd9bfc0f8056e)), closes [#5](https://gitlab.com/HeinousTugboat/transistors-inc/issues/5)
* updates page title to "Transistors Inc" ([e96bb7b](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e96bb7b3dff3d7cc9547286bf37bb02d6552192d))
* **transistors.service:** adds minor tweaks to accumulated transistors and display ([21f1d86](https://gitlab.com/HeinousTugboat/transistors-inc/commit/21f1d86e10c6ab14309834735946990c68aa4a27))
* **transistors.service:** moves initial transistor to seed transistor, updates acc. transistors ([ab638fc](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ab638fcad40af69ba1675bf8dcb1fd9147c48fc2)), closes [#7](https://gitlab.com/HeinousTugboat/transistors-inc/issues/7)
* **transistors.service:** updates default INCREASE_R state to 1.5x instead of 2x ([2b555d0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2b555d08a35ed7b737444883e083781d387bdbc2))


### Bug Fixes

* **ai.service:** fixes importing findTier from wrong place ([307cbb8](https://gitlab.com/HeinousTugboat/transistors-inc/commit/307cbb821b0ee12376ed39d44a766d49237711b4))
* **e2e:** updates e2e test to reflect 0.0 transistor on load ([e5e219a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e5e219a341d4239657c5cb2d9755a69173e54945))
* **romu-random:** updates flaky test that only checks 100 values vs 100k ([ed79b3f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ed79b3f08b86a2bfaf99ae215358e6d0127e88b9))
* **transistors.service:** fixes compute calculation so it should never drop negative ([3ccd7aa](https://gitlab.com/HeinousTugboat/transistors-inc/commit/3ccd7aa2e177ec865c4adddd094b05de8fecb58f)), closes [#17](https://gitlab.com/HeinousTugboat/transistors-inc/issues/17)
* **units.pipe:** updates units pipe to be polymorphic for decimal and number ([55a1a4f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/55a1a4f899d1b4fdfbfc50f17cca6488bdf58ebf))
* moved break_infinity to vendor folder, added declarations file from repo ([0b1fc1c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/0b1fc1c04a1922b7abd227dad08c0d2d286936b9))
* **transistors.service:** updates m to reset at the beginning of each round ([6832afb](https://gitlab.com/HeinousTugboat/transistors-inc/commit/6832afbe8917f8ecffff25a68fff0e5a546a6081)), closes [#10](https://gitlab.com/HeinousTugboat/transistors-inc/issues/10)

### [0.0.1-alpha.4](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.3...v0.0.1-alpha.4) (2020-05-27)


### Features

* **app.component:** refactors round timer to use new progress bar component ([e0ffd8f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e0ffd8fdcde0f37358d2e6513db91a66107316d5))
* **diagnostics.component:** splits diagnostics into separate floating component ([13d68bb](https://gitlab.com/HeinousTugboat/transistors-inc/commit/13d68bb574679505880af64d874e0351c822e8ad))
* **log.component:** reverses event log order, fades items so only 15 most recent are visible ([c31598c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/c31598c26bd434a02624b973560d1140dcce72d5))
* **progress.component:** adds progress component to show dynamic width bars ([ad37912](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ad379126919c4b5ee97c5eacde9bdca719fe0206))
* **transistors.service:** adds minor tweaks to accumulated transistors and display ([21f1d86](https://gitlab.com/HeinousTugboat/transistors-inc/commit/21f1d86e10c6ab14309834735946990c68aa4a27))
* **transistors.service:** moves initial transistor to seed transistor, updates acc. transistors ([ab638fc](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ab638fcad40af69ba1675bf8dcb1fd9147c48fc2)), closes [#7](https://gitlab.com/HeinousTugboat/transistors-inc/issues/7)
* **transistors.service:** updates default INCREASE_R state to 1.5x instead of 2x ([2b555d0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2b555d08a35ed7b737444883e083781d387bdbc2))


### Bug Fixes

* **e2e:** updates e2e test to reflect 0.0 transistor on load ([e5e219a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e5e219a341d4239657c5cb2d9755a69173e54945))
* **transistors.service:** updates m to reset at the beginning of each round ([6832afb](https://gitlab.com/HeinousTugboat/transistors-inc/commit/6832afbe8917f8ecffff25a68fff0e5a546a6081)), closes [#10](https://gitlab.com/HeinousTugboat/transistors-inc/issues/10)

### [0.0.1-alpha.3](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.2...v0.0.1-alpha.3) (2020-05-26)


### Features

* **app.component:** moved train/sell buttons to be side by side ([9b89571](https://gitlab.com/HeinousTugboat/transistors-inc/commit/9b89571278f3d29ed109a7cf8f9bdaf8af648e6e))
* **app.component:** rerannges state buttons, changes default state ([a64bc5f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a64bc5f0aefbede421f1ed4fa92a3f2ea63d575e))
* **transistors.service:** adds accumulated transistors to track total earned ([7d997dd](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7d997dd06e383278aba8beed6fe186271b76b486)), closes [#2](https://gitlab.com/HeinousTugboat/transistors-inc/issues/2)
* **transistors.service:** reduces default r to where the first round only gets to about 1.2 ([2ef605a](https://gitlab.com/HeinousTugboat/transistors-inc/commit/2ef605a13372df76c3fd995b2c10bfa94ec5fb3c))
* **transistors.service:** updates compute formula to match transistor gain formula ([a5b5f36](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a5b5f369f1c3f885f58bcd1ead942b4168bd767b))
* **transistors.service:** updates formula to use stable continously compounding formula ([f825845](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f825845eef210ebdcb6de4ed5f9518661ca17b78))
* **transistors.service:** updates sell logic and refactors trainedAI to consumedTransisors ([1b28dec](https://gitlab.com/HeinousTugboat/transistors-inc/commit/1b28decfe540bd0d0bbfda2115edb136bb447384))
* **transistors.service:** updates to autosell transistors if starting a round without using them ([ae11c26](https://gitlab.com/HeinousTugboat/transistors-inc/commit/ae11c26d8661f78e207fdff3e3b4bb81edf8e059))
* **upgrades.service:** adds initial implementation pass for upgrades ([e25a342](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e25a3429db97962f694e9358ede6874b64089fba))
* **utils:** adds assert-never ([e40c0a1](https://gitlab.com/HeinousTugboat/transistors-inc/commit/e40c0a117a91f23b4a69be22a2b07f246625d97f))
* **utils:** adds clamp function with tests ([39e6c85](https://gitlab.com/HeinousTugboat/transistors-inc/commit/39e6c8524186c16f2a5a38bd21e8a5c3d6679bb2))

### [0.0.1-alpha.2](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.1...v0.0.1-alpha.2) (2020-05-25)


### Features

* **ai:** adds ability to train AIs, rounds out code for building AIs ([7ac141d](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7ac141d65b8c5c13af1fa149741661f022b91b3c))
* **ais.component:** adds display for in progress AI ([4ebcff7](https://gitlab.com/HeinousTugboat/transistors-inc/commit/4ebcff7286ba3ca14c479764fdd431c18ba40c23))
* adds booleans to transistors and ai services ([a660fc0](https://gitlab.com/HeinousTugboat/transistors-inc/commit/a660fc0ecce94e039b6d6552f6c5ab0676b12517))
* **ai:** refactors ai services and components into folder ([dd937b9](https://gitlab.com/HeinousTugboat/transistors-inc/commit/dd937b9f9092afdd33fd32ea943f1fe7acc28552))
* **log.service:** adds log and logger service ([d8b2624](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d8b2624c0522341d4ab4d7ea61fabd62cdba757b))
* **not-found.component:** adds not-found component and path ([89cd0b7](https://gitlab.com/HeinousTugboat/transistors-inc/commit/89cd0b7af8ccf2f4a6531f098439b0ea399dabf1))
* **upgrade:** adds initial upgrade service and upgrades component ([d970ae3](https://gitlab.com/HeinousTugboat/transistors-inc/commit/d970ae300cfd1f70ad3ec3d97ec8f5a4d5e1883c))
* adds changelog view! ([64a6447](https://gitlab.com/HeinousTugboat/transistors-inc/commit/64a6447e65fc624921a36b039733848fe906328e))
* adds font-awesome icons in a couple places ([23b085f](https://gitlab.com/HeinousTugboat/transistors-inc/commit/23b085f55642728db2694b4894ae9438d97dcf07))


### Bug Fixes

* **transistors.service:** fixes credits resetting on every sale ([bb78060](https://gitlab.com/HeinousTugboat/transistors-inc/commit/bb78060ea492ec3648faa5c0a6b218e4718f9cc9))
* **transistors.service:** fixes transistors not resetting at the beginning of each round ([28b7ba4](https://gitlab.com/HeinousTugboat/transistors-inc/commit/28b7ba4336cc251df4681abb6ad8d081198d9109))
* fixes favicon hrefs to work with gitlab ([dff9f2c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/dff9f2c58732d394a2303348910049fd38e01dbf))

### [0.0.1-alpha.1](https://gitlab.com/HeinousTugboat/transistors-inc/compare/v0.0.1-alpha.0...v0.0.1-alpha.1) (2020-05-24)


### Features

* adds heavy styling and layout changes, initial theming pass! ([99450fe](https://gitlab.com/HeinousTugboat/transistors-inc/commit/99450fe539f9050d88c33ebf23d50adcc52414d2))
* **ai.service:** adds initial pass at AIs that modify your percentages, decoupled base rates ([cc16094](https://gitlab.com/HeinousTugboat/transistors-inc/commit/cc16094c1dc6ef3ecd2db86dad53f60266a992f2))
* **transistors.service:** adds ability to upgrade length by selling transistors ([f5b0be8](https://gitlab.com/HeinousTugboat/transistors-inc/commit/f5b0be8a7841b9bb5e5797328feea584316036b7))
* adds design and home components, basic routes ([7d534d2](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7d534d22ed6cb87194c2d3d9197cdc6850a76168))
* adds version to app component ([7d74815](https://gitlab.com/HeinousTugboat/transistors-inc/commit/7d748159537202d8bde1c1e8a4aaa26c6cfbceab))

### [0.0.1-alpha.0](https://gitlab.com/HeinousTugboat/transistors-inc/compare/5d84a1cb9729b576093fd0c15af9e4be93d649d9...v0.0.1-alpha.0) (2020-05-23)


### Features

* **loop.service:** adds initial loop service and interface ([5d84a1c](https://gitlab.com/HeinousTugboat/transistors-inc/commit/5d84a1cb9729b576093fd0c15af9e4be93d649d9))
* **transistors.service:** adds initial transistors service ([500fcca](https://gitlab.com/HeinousTugboat/transistors-inc/commit/500fccae42ca1217a0671f3d694b8afff6e51a25))
