// import { browser, by, element, logging } from 'protractor';
import { browser, logging } from 'protractor';

import { AppPage } from './app.po';

describe('Transistors Inc App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display 0 transistors', () => {
    page.navigateTo();
    browser.waitForAngularEnabled(false);
    expect(page.getTransistorAmount()).toEqual('0');
  });

  describe('one round in Increase Speed state', () => {
    let transistors: Promise<string>;
    let cycles: Promise<string>;
    let chips: Promise<string>;

    beforeAll(() => {
      page.navigateTo();
      browser.waitForAngularEnabled(false);
      page.clickRoundStart();
      browser.sleep(4000);
      transistors = page.getTransistorAmount();
      cycles = page.getCycleAmount();
      chips = page.getChipAmount();
      browser.sleep(500);
    });

    it('should earn 107 transistors', () => {
      expect(page.getTransistorAmount()).toEqual(transistors);
      expect(page.getTransistorAmount()).toEqual('107');
    });

    it('should earn 12.9 cycles', () => {
      expect(page.getCycleAmount()).toEqual(cycles);
      expect(page.getCycleAmount()).toEqual('12.9');
    });

    it('should earn 1 chip', () => {
      expect(page.getChipAmount()).toEqual(chips);
      expect(page.getChipAmount()).toEqual('1');
    });
  });

  describe('one round in Increase R state', () => {
    let transistors: Promise<string>;
    let cycles: Promise<string>;
    let chips: Promise<string>;

    beforeAll(() => {
      page.navigateTo();
      browser.waitForAngularEnabled(false);
      page.clickReplicationState();
      page.clickRoundStart();
      browser.sleep(16000);
      transistors = page.getTransistorAmount();
      cycles = page.getCycleAmount();
      chips = page.getChipAmount();
      browser.sleep(500);
    });

    it('should earn 195 transistors', () => {
      expect(page.getTransistorAmount()).toEqual(transistors);
      expect(page.getTransistorAmount()).toEqual('195');
    });

    it('should earn 18.2 cycles', () => {
      expect(page.getCycleAmount()).toEqual(cycles);
      expect(page.getCycleAmount()).toEqual('18.2');
    });

    it('should earn 1 chip', () => {
      expect(page.getChipAmount()).toEqual(chips);
      expect(page.getChipAmount()).toEqual('1');
    });
  });

  describe('one round in Increase M state', () => {
    let transistors: Promise<string>;
    let cycles: Promise<string>;
    let chips: Promise<string>;

    beforeAll(() => {
      page.navigateTo();
      browser.waitForAngularEnabled(false);
      page.clickCapState();
      page.clickRoundStart();
      browser.sleep(16000);
      transistors = page.getTransistorAmount();
      cycles = page.getCycleAmount();
      chips = page.getChipAmount();
      browser.sleep(500);
    });

    it('should earn 107 transistors', () => {
      expect(page.getTransistorAmount()).toEqual(transistors);
      expect(page.getTransistorAmount()).toEqual('107');
    });

    it('should earn 12.9 cycles', () => {
      expect(page.getCycleAmount()).toEqual(cycles);
      expect(page.getCycleAmount()).toEqual('12.9');
    });

    it('should earn 1 chip', () => {
      expect(page.getChipAmount()).toEqual(chips);
      expect(page.getChipAmount()).toEqual('1');
    });
  });

  describe('dynamically changing state', () => {
    let transistors: Promise<string>;
    let cycles: Promise<string>;
    let chips: Promise<string>;

    beforeAll(() => {
      page.navigateTo();
      browser.waitForAngularEnabled(false);
      page.clickRoundStart();
      browser.sleep(500);
      page.clickReplicationState();
      browser.sleep(500);
      page.clickCapState();
      browser.sleep(500);
      page.clickReplicationState();
      browser.sleep(500);
      page.clickSpeedState();
      browser.sleep(500);
      page.clickCapState();
      browser.sleep(500);
      page.clickSpeedState();
      browser.sleep(2500);
      transistors = page.getTransistorAmount();
      cycles = page.getCycleAmount();
      chips = page.getChipAmount();
    });

    it('should earn 113 transistors', () => {
      expect(page.getTransistorAmount()).toEqual(transistors);
      expect(page.getTransistorAmount()).toBeCloseTo(113, -1);
    });

    it('should earn 14 cycles', () => {
      expect(page.getCycleAmount()).toEqual(cycles);
      expect(page.getCycleAmount()).toBeCloseTo(14.0, -1);
    });

    it('should earn 1 chip', () => {
      expect(page.getChipAmount()).toEqual(chips);
      expect(page.getChipAmount()).toEqual('1');
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);

    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));

    // Clear local storage after each test.
    browser.executeScript('window.localStorage.clear()');
  });
});
