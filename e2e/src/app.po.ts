import { browser, by, element } from 'protractor';

export class AppPage {
  public navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  public getTransistorAmount(): Promise<string> {
    return element(by.id('transistor-count')).getText() as Promise<string>;
  }

  public getCycleAmount(): Promise<string> {
    return element(by.id('cycle-count')).getText() as Promise<string>;
  }

  public getChipAmount(): Promise<string> {
    return element(by.id('chip-count')).getText() as Promise<string>;
  }

  public clickRoundStart(): Promise<void> {
    return element(by.id('round-start-button')).click() as Promise<void>;
  }

  public clickSpeedState(): Promise<void> {
    return element(by.id('increase-speed-button')).click() as Promise<void>;
  }

  public clickReplicationState(): Promise<void> {
    return element(by.id('increase-replication-button')).click() as Promise<void>;
  }

  public clickCapState(): Promise<void> {
    return element(by.id('increase-cap-button')).click() as Promise<void>;
  }
}
