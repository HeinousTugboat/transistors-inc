export interface SliderBar {
  label: string;
  value: number;
  lock: boolean;
}

export const enum ProgressBarType {
  ROUND_TIMER = 'round-timer'
}
