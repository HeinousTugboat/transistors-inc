export const enum Currency {
  CREDIT = 'credit',
  CYCLE = 'cycle',
  TRANSISTOR = 'transistor',
  CHIP = 'chip',
  BONUS_TICK = 'bonus tick',
  FLOP = 'flop',
}
