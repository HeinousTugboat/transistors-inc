import { Currency, Mod, ModCategory, ModType } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';

export interface UpgradeCost {
  currency: Currency;
  amount: Decimal;
}
export type CreditModifier = ModType.INCREASE_ROUND_LENGTH
  | ModType.MULTIPLY_REPLICATION;

export type ChipModifier = ModType.MULTIPLY_CAP
  | ModType.MULTIPLY_CAP_GROWTH
  | ModType.DECREASE_AUTOBUY_DELAY;

export type CycleModifier = ModType.MULTIPLY_REPLICATION
  | ModType.MULTIPLY_CAP
  | ModType.MULTIPLY_CAP_GROWTH;

export type UpgradeModifier = CreditModifier | CycleModifier | ChipModifier;
export type UpgradeModCategory = ModCategory.CYCLE | ModCategory.CREDIT | ModCategory.CHIP;

export interface UpgradeMod extends Mod {
  category: UpgradeModCategory;
}

export interface Upgrade {
  id: string;
  level: Decimal;
  name: string;
  cost: UpgradeCost;
  mod: UpgradeMod;
  repeating: boolean;
  generator?: (level: Decimal) => Upgrade;
  requirements: Upgrade[];
  description: (amount: Decimal) => string;
}

export interface UpgradeGeneratorConfig {
  source: Upgrade[];
  category: UpgradeModCategory;
  currency: Currency;
  name: (level: Decimal) => string;
  type: UpgradeModifier;
  modAmount: Decimal;
  costAmount?: (level: Decimal) => Decimal;
  repeating?: boolean;
  descriptionText?: string;
  description?: (amount: Decimal) => string;
}

export type PartialGeneratorConfig = Pick<UpgradeGeneratorConfig, 'source' | 'name' | 'type' | 'modAmount'>;
