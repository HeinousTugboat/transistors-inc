export interface OddsItem<T> {
  item: T;
  odds: number;
  counter?: number;
}
