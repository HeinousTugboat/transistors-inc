import { Decimal } from '@vendor';

export enum ModType {
  IMPROVE_AI_PROMOTION = 'improve_ai_promotion',
  DECREASE_AUTOBUY_DELAY = 'decrease_autobuy_delay',
  ELEVATE_CYCLE_GENERATION = 'elevate_cycle_generation',
  ELEVATE_CREDIT_EXCHANGE = 'elevate_credit_exchange',
  IMPROVE_CAP = 'improve_cap',
  IMPROVE_CAP_GROWTH = 'improve_cap_growth',
  IMPROVE_CREDIT_EXCHANGE = 'improve_credit_exchange',
  IMPROVE_REPLICATION = 'improve_replication',
  INCREASE_ROUND_LENGTH = 'increase_round_length',
  MULTIPLY_CAP = 'multiply_cap',
  MULTIPLY_CAP_GROWTH = 'multiply_cap_growth',
  MULTIPLY_CYCLE_GENERATION = 'multiply_cycle_generation',
  MULTIPLY_CREDIT_EXCHANGE = 'multiply_credit_exchange',
  MULTIPLY_REPLICATION = 'multiply_replication',
}

export enum ModCategory {
  AI = 'ai',
  CREDIT = 'credit',
  CYCLE = 'cycle',
  CHIP = 'chip',
  SYSTEM = 'system',
}

export interface Mod {
  category: ModCategory;
  type: ModType;
  amount: Decimal;
}

export interface ModProvider {
  modList: Set<Mod>;
}
