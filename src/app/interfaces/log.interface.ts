export enum LogLevel {
  INFO = 'info',
  ERROR = 'error'
}

export interface LogItem {
  id: string;
  text: string;
  time: Date;
  level: LogLevel;
}

export interface LogJson {
  id: string;
  text: string;
  time: string;
  level: string;
}
