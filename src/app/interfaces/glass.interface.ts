export enum GlassLabel {
  NONE = '',
  CHIPS = 'chips',
  BONUS_TICKS = 'bonus_ticks',
  CYCLES = 'cycles',
  CREDITS = 'credits',
  GENERATE_AI = 'generate_ai',
  HOME_INFO = 'home_info',
  LOG = 'log',
  NAV = 'nav',
  SELL_BUTTONS = 'sell_buttons',
  START_ROUND = 'start_round',
  TRANSISTOR_STATE = 'transistor_state',
  TRANSISTORS = 'transistors',
}

export interface GlassElement {
  label: GlassLabel;
  update(active: boolean): void;
}
