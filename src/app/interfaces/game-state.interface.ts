import { AiJson } from './ai.interface';
import { LogJson } from './log.interface';

export interface BaseGameState {
  lastSavedAt: string;
  gameTime: string;
  version: string;
}

export interface RandomState {
  seed?: [number, number];
}

export interface AiState {
  ai: AiJson[];
}

export interface UpgradeState {
  upgradeIds: string[];
}

export interface CurrencyState {
  chip: string;
  credit: string;
  bonusTick: string;
}

export interface OptionsState {
  options: {
    format: boolean;
    logLength: number;
    saveInterval: number;
    debug: boolean;
    cheatState: boolean;
    showSocialMedia: boolean;
    showIdleAllocation: boolean;
  };
  aiAllocation?: number;
  researchAllocation?: number;
}

export interface LogState {
  log: LogJson[];
}

export type GameState =
  BaseGameState
  & RandomState
  & AiState
  & UpgradeState
  & CurrencyState
  & OptionsState
  & LogState;

export type GameStateSlice =
  BaseGameState
  | RandomState
  | AiState
  | UpgradeState
  | CurrencyState
  | OptionsState
  | LogState;

export interface Serializable<T extends GameStateSlice> {
  state: T;
}
