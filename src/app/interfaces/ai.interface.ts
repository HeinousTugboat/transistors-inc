import { Decimal } from '@vendor';

import { Mod, ModType } from './mod.interface';

export const enum AiTier {
  UNDERGRAD,    // White
  GRAD,         // Green
  PROFESSOR,    // Blue
  NOBEL,        // Purple
  BREAKTHROUGH, // Orange
  NUM_TIERS
}

export type AiModifier = ModType.MULTIPLY_CAP
  | ModType.MULTIPLY_CAP_GROWTH
  | ModType.MULTIPLY_CYCLE_GENERATION
  | ModType.MULTIPLY_CREDIT_EXCHANGE
  | ModType.MULTIPLY_REPLICATION
  | ModType.ELEVATE_CYCLE_GENERATION
  | ModType.ELEVATE_CREDIT_EXCHANGE;


export const enum AiModifierRarity {
  NORMAL,
  SPECIAL
}

export interface AiModifierData {
  min: number;
  spread: number;
}

export interface AiMod extends Mod {
  type: AiModifier;
}

export interface Ai {
  id: string;
  tier: AiTier;
  lines: AiMod[];
  baseCompute: Decimal;
  trainedCompute: Decimal;
  // trainingProfile: TrainingAi;
}

export interface BaseTrainingAi {
  start: Decimal;
  end?: Decimal;
  initialChips: Decimal;
  currentCompute: Decimal;
  currentTime: Decimal;
  // targetCompute?: Decimal;
  // targetTime?: Decimal;
  // Maybes.
  tier?: AiTier;
  ai?: Ai;
  setLines?: AiMod[];
}

export interface TimeTargetedTrainingAi extends BaseTrainingAi {
  targetTime: Decimal;
}

export interface ComputeTargetedTrainingAi extends BaseTrainingAi {
  targetCompute: Decimal;
}

export type TrainingAi = TimeTargetedTrainingAi | ComputeTargetedTrainingAi;

export interface AiJson {
  id: string;
  readyTime: string;
  startTime: string;
  complete: boolean;
  transistors: string;
  tier: number;
  lines: string[];
}
