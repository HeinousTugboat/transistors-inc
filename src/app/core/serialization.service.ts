import { Inject, Injectable, InjectionToken } from '@angular/core';
import { GameState, GameStateSlice, Serializable } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';
import { environment } from 'src/environments/environment';

import { initialGameState } from './data/initial-game-state.data';
import { MigrationService } from './migration.service';

export const BROWSER_STORAGE = new InjectionToken<Storage>('Browser Storage', {
  providedIn: 'root',
  factory: () => localStorage
});

@Injectable({
  providedIn: 'root',
})
export class SerializationService {
  private readonly storageKey = environment.storageKey;
  private registeredServices: Set<Serializable<GameStateSlice>> = new Set();
  private state: GameState = initialGameState;

  constructor(
    @Inject(BROWSER_STORAGE) private storage: Storage,
    private migration: MigrationService
  ) {
    this.loadState();
  }

  public register(service: Serializable<GameStateSlice>): void {
    this.registeredServices.add(service);

    try {
      service.state = this.state;
    } catch (e) {
      console.error('Error rehydrating game state on service registration', e, service, this.state);
      service.state = initialGameState;
    } finally {
      this.state = {
        ...this.state,
        ...service.state
      };
    }
  }

  public saveState(time: Decimal): void {
    this.state.lastSavedAt = Date.now().toString();
    this.state.gameTime = time.toString();

    for (const service of this.registeredServices) {
      this.state = {
        ...this.state,
        ...service.state
      };
    }

    this.serializeState(this.state);
  }

  public loadState(data?: string): GameState | null {
    let state = this.deserializeState(data);

    if (state !== null) {
      for (const service of this.registeredServices) {
        try {
          service.state = state;
        } catch (e) {
          console.error('Error rehydrating game state on load', e, service, state);
          service.state = initialGameState;
        } finally {
          state = {
            ...state,
            ...service.state
          };
        }
      }
    }

    return state;
  }

  public resetState(): void {
    this.serializeState({ ...initialGameState });
    this.loadState();
  }

  private serializeState(state: GameState): void {
    this.state = state;

    this.storage.setItem(this.storageKey, JSON.stringify(state));
  }

  private deserializeState(data?: string): GameState | null {
    let stateString: unknown = data;

    if (stateString === undefined) {
      stateString = this.storage.getItem(this.storageKey);
    }

    if (typeof stateString === 'string') {
      try {
        const parsedState: unknown = JSON.parse(stateString);

        this.state = this.migration.fromVersion(parsedState) ?? initialGameState;
      } catch (err) {
        console.error(err);
      }
    }

    return this.state;
  }
}
