import { Injectable } from '@angular/core';
import { RandomState, Serializable } from '@transistors-inc/interfaces';
import { Decimal, RNG, romDuoJrRandom } from '@vendor';

import { SerializationService } from './serialization.service';

@Injectable({
  providedIn: 'root'
})
export class RandomService implements Serializable<RandomState> {
  private rand!: RNG;

  constructor(serialization: SerializationService) {
    this.init();
    serialization.register(this);
  }

  get seed(): number[] {
    return this.rand.seed;
  }

  public reseed(seed?: [number, number]): void {
    this.rand = romDuoJrRandom(seed);
  }

  public init(seed: number = Date.now()): void {
    this.rand = romDuoJrRandom();
    this.rand.init(seed);
  }

  public next(): number { return this.rand(); }
  public nextBig(): BigInt { return BigInt(this.rand()); }
  public nextDec(): Decimal { return new Decimal(this.rand()); }
  public range(n: number): number { return this.rand.range(n); }

  get state(): RandomState {
    return { seed: this.rand.seed };
  }

  set state(value: RandomState) {
    this.reseed(value.seed);
  }
}
