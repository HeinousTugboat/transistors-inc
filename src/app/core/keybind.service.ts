import { Injectable, OnDestroy } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import { fromEventPattern, merge, Observable } from 'rxjs';

export type KeyboardHandler = (ev: KeyboardEvent) => void;

@Injectable({
  providedIn: 'root'
})
export class KeybindService implements OnDestroy {
  private keyDown$: Observable<KeyboardEvent>;
  private keyUp$: Observable<KeyboardEvent>;
  public readonly key$: Observable<KeyboardEvent>;

  private removeKeydownHandler: () => void = () => { };
  private removeKeyupHandler: () => void = () => { };

  constructor(private events: EventManager) {
    this.keyDown$ = fromEventPattern(this.addKeydownHandler.bind(this), this.removeKeydownHandler.bind(this));
    this.keyUp$ = fromEventPattern(this.addKeyupHandler.bind(this), this.removeKeyupHandler.bind(this));
    this.key$ = merge(this.keyDown$, this.keyUp$);
  }

  public ngOnDestroy(): void {
    this.removeKeydownHandler();
    this.removeKeyupHandler();
  }

  private addKeydownHandler(handler: KeyboardHandler): void {
    this.removeKeydownHandler = this.events.addGlobalEventListener('window', 'keydown', handler) as () => void;
  }

  private addKeyupHandler(handler: KeyboardHandler): void {
    this.removeKeyupHandler = this.events.addGlobalEventListener('window', 'keyup', handler) as () => void;
  }
}
