import { GameState } from '@transistors-inc/interfaces';
import { environment } from 'src/environments/environment';

export const initialGameState: GameState = {
  version: environment.version,
  bonusTick: '0',
  upgradeIds: [],
  credit: '0',
  chip: '0',
  lastSavedAt: Date.now().toString(),
  gameTime: '0',
  ai: [],
  log: [],
  options: {
    format: true,
    logLength: 15,
    saveInterval: 5000,
    debug: false,
    cheatState: false,
    showSocialMedia: true,
    showIdleAllocation: true,
  },
  aiAllocation: undefined,
  researchAllocation: undefined
};
