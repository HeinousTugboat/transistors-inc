import { Injectable } from '@angular/core';
import { Currency, Loop } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';

import { CurrencyService } from './currency.service';
import { LoopService } from './loop.service';

@Injectable({
  providedIn: 'root'
})
export class BonusTicksService {
  public enabled = false;
  public multiplier = new Decimal(100);

  constructor(private currency: CurrencyService, loop: LoopService) {
    loop.loop$.subscribe(l => this.update(l));
  }

  private update({ offset }: Loop): void {
    this.currency.add(Currency.BONUS_TICK, offset);
  }

  public available(): boolean {
    return this.currency[Currency.BONUS_TICK].gt(0);
  }

  public toggle(): void {
    this.enabled = !this.enabled;
  }
}
