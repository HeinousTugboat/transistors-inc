import { Injectable } from '@angular/core';
import { GameState } from '@transistors-inc/interfaces';
import { isVersionedGameState } from '@transistors-inc/utils';
import { environment } from 'src/environments/environment';

import { initialGameState } from './data/initial-game-state.data';

@Injectable({
  providedIn: 'root',
})
export class MigrationService {
  public fromVersion(state: unknown): GameState | null {
    if (!isVersionedGameState(state)) {
      return null;
    }

    switch (state.version) {
      // Add additional versions here for migrating that state to current game state
      case environment.version:
      default:
        return {
          version: environment.version,
          seed: state.seed,
          bonusTick: state.bonusTick ?? initialGameState.bonusTick,
          upgradeIds: Array.isArray(state.upgradeIds) ? state.upgradeIds : initialGameState.upgradeIds,
          chip: state.chip ?? initialGameState.chip,
          credit: state.credit ?? initialGameState.credit,
          lastSavedAt: state.lastSavedAt ?? Date.now().toString(),
          gameTime: state.gameTime ?? initialGameState.gameTime,
          ai: state.ai ?? [],
          log: state.log ?? [],
          options: {
            format: state.options?.format ?? initialGameState.options.format,
            logLength: state.options?.logLength ?? initialGameState.options.logLength,
            saveInterval: state.options?.saveInterval ?? initialGameState.options.saveInterval,
            debug: state.options?.debug ?? initialGameState.options.debug,
            cheatState: state.options?.cheatState ?? initialGameState.options.cheatState,
            showSocialMedia: state.options?.showSocialMedia ?? initialGameState.options.showSocialMedia,
            showIdleAllocation: state.options?.showIdleAllocation ?? initialGameState.options.showIdleAllocation,
          },
          aiAllocation: state.aiAllocation ?? initialGameState.aiAllocation,
          researchAllocation: state.researchAllocation ?? initialGameState.researchAllocation,
        };
    }
  }
}
