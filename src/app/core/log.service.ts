import { Injectable } from '@angular/core';
import { LogItem, LogLevel, LogState, Serializable } from '@transistors-inc/interfaces';

import { SerializationService } from './serialization.service';

@Injectable({
  providedIn: 'root'
})
export class LogService implements Serializable<LogState> {
  constructor(serialization: SerializationService) {
    serialization.register(this);
  }

  public history: LogItem[] = [];
  private nextId = 1;

  public add(text: string, level: LogLevel = LogLevel.INFO): void {
    const logItem: LogItem = { text, id: `item-${this.nextId++}`, time: new Date(), level };
    this.history.push(logItem);
    // console.log(`Log: ${JSON.stringify(logItem)}`);
  }

  public error(text: string): void {
    this.add(text, LogLevel.ERROR);
  }

  get state(): LogState {
    return {
      log: this.history.map(item => ({
        id: item.id,
        text: item.text,
        time: item.time.toISOString(),
        level: item.level
      }))
    };
  }

  set state(value: LogState) {
    this.history = value.log.map(item => ({
      id: item.id,
      text: item.text,
      time: new Date(item.time),
      level: item.level as LogLevel,
    }));
    this.nextId = Math.max(...this.history.map(item => parseInt(item.id.split('-')[1], 10)));
    this.add('Restoring from save');
  }
}
