import { TestBed } from '@angular/core/testing';

import { CheatGuard } from './cheat.guard';

describe('CheatGuard', () => {
  let guard: CheatGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CheatGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
