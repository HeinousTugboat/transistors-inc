import { Injectable } from '@angular/core';
import { Currency, CurrencyState, Serializable } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';

import { initialGameState } from './data/initial-game-state.data';
import { SerializationService } from './serialization.service';

const CREDIT: unique symbol = Symbol.for(Currency.CREDIT);
const CYCLE: unique symbol = Symbol.for(Currency.CYCLE);
const TRANSISTOR: unique symbol = Symbol.for(Currency.TRANSISTOR);
const CHIP: unique symbol = Symbol.for(Currency.CHIP);
const BONUS_TICK: unique symbol = Symbol.for(Currency.BONUS_TICK);
const FLOP: unique symbol = Symbol.for(Currency.FLOP);

@Injectable({
  providedIn: 'root'
})
export class CurrencyService implements Serializable<CurrencyState> {
  constructor(serialization: SerializationService) {
    serialization.register(this);
  }

  private [CREDIT] = new Decimal(initialGameState.credit);
  private [CHIP] = new Decimal(initialGameState.chip);
  private [BONUS_TICK] = new Decimal(initialGameState.bonusTick);
  private [CYCLE] = new Decimal(0);
  private [TRANSISTOR] = new Decimal(0);
  private [FLOP] = new Decimal(0);

  private get [Currency.TRANSISTOR](): Decimal { return this[TRANSISTOR]; }
  private set [Currency.TRANSISTOR](v: Decimal) {
    if (v.lt(0)) { throw new RangeError(`Trying to set Transistor negative! current: ${this[TRANSISTOR]}, v: ${v}`); }
    this[TRANSISTOR] = v;
  }

  private get [Currency.CHIP](): Decimal { return this[CHIP]; }
  private set [Currency.CHIP](v: Decimal) {
    // tslint:disable-next-line: max-line-length
    if (v.lt(0)) { throw new RangeError(`Trying to set Chip negative! current: ${this[CHIP]}, v: ${v}`); }
    this[CHIP] = v;
  }

  private get [Currency.CYCLE](): Decimal { return this[CYCLE]; }
  private set [Currency.CYCLE](v: Decimal) {
    if (v.lt(0)) { throw new RangeError(`Trying to set Cycle negative! current: ${this[CYCLE]}, v: ${v}`); }
    this[CYCLE] = v;
  }

  private get [Currency.CREDIT](): Decimal { return this[CREDIT]; }
  private set [Currency.CREDIT](v: Decimal) {
    if (v.lt(0)) { throw new RangeError(`Trying to set Credit negative! current: ${this[CREDIT]}, v: ${v}`); }
    this[CREDIT] = v;
  }

  private get [Currency.BONUS_TICK](): Decimal { return this[BONUS_TICK]; }
  private set [Currency.BONUS_TICK](v: Decimal) {
    if (v.lt(0)) { throw new RangeError(`Trying to set Bonus Tick negative! current: ${this[BONUS_TICK]}, v: ${v}`); }
    this[BONUS_TICK] = v;
  }

  private get [Currency.FLOP](): Decimal { return this[FLOP]; }
  private set [Currency.FLOP](v: Decimal) {
    if (v.lt(0)) { throw new RangeError(`Trying to set FLOP negative! current: ${this[FLOP]}, v: ${v}`); }
    this[FLOP] = v;
  }

  public canAfford(currency: Currency, amount: Decimal): boolean {
    return this[currency].gte(amount);
  }

  public add(currency: Currency, amount: Decimal, max?: Decimal): void {
    if (amount.lt(0)) { throw new RangeError(`Cannot add ${amount} currency to ${currency}`); }
    if (max !== undefined && this[currency].add(amount).gt(max)) {
      this[currency] = new Decimal(max);
    } else {
      this[currency] = this[currency].add(amount);
    }
  }

  public subtract(currency: Currency, amount: Decimal): void {
    if (amount.lt(0)) { throw new RangeError(`Cannot subtract ${amount} currency from ${currency}`); }
    if (amount.gt(this[currency])) {
      // tslint:disable-next-line: max-line-length
      throw new Error(`Cannot afford to subtract ${amount} from ${currency}, only ${this[currency]} available`);
    }

    this[currency] = this[currency].sub(amount);
  }

  public set(currency: Currency, amount: Decimal): void {
    if (amount.lt(0)) { throw new RangeError(`Cannot set ${currency} to negative amount ${amount}`); }

    this[currency] = new Decimal(amount);
  }

  get state(): CurrencyState {
    return {
      credit: this[Currency.CREDIT].toString(),
      chip: this[Currency.CHIP].toString(),
      bonusTick: this[Currency.BONUS_TICK].toString(),
    };
  }

  set state(value: CurrencyState) {
    this[Currency.CREDIT] = new Decimal(value.credit);
    this[Currency.CHIP] = new Decimal(value.chip);
    this[Currency.BONUS_TICK] = new Decimal(value.bonusTick);
    this[Currency.CYCLE] = new Decimal(0);
    this[Currency.TRANSISTOR] = new Decimal(0);
  }
}
