import { Injectable } from '@angular/core';
import { Loop } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';
import {
  animationFrameScheduler,
  ConnectableObservable,
  EMPTY,
  iif,
  Observable,
  scheduled,
  Subject
} from 'rxjs';
import { map, publish, repeat, switchMap, tap, timeInterval } from 'rxjs/operators';

import { LogService } from './log.service';
import { OptionsService } from './options.service';
import { SerializationService } from './serialization.service';

export const MS_PER_SEC = new Decimal(1_000);

// tslint:disable: no-console
@Injectable({
  providedIn: 'root'
})
export class LoopService {
  private readonly running$: Subject<boolean> = new Subject();
  private loopSource$: Observable<Loop>;
  // tslint:disable: variable-name
  private _tick = new Decimal(0);
  private _time = new Decimal(performance.now());
  private _start = new Decimal(performance.now());
  private _elapsedTime = new Decimal(0);
  // tslint:enable: variable-name
  private stepOnce = false;
  public loop$: ConnectableObservable<Loop>;

  private savedAt: Decimal | null = null;

  constructor(
    log: LogService,
    private options: OptionsService,
    private serialization: SerializationService
  ) {
    this.loopSource$ = scheduled([performance.now()], animationFrameScheduler).pipe(
      map(() => performance.now()),
      repeat(),
      timeInterval(animationFrameScheduler),
      map(({ value, interval }) => ({ value: new Decimal(value), interval: new Decimal(interval) })),
      map(({ value, interval }) => {
        if (interval.gt(1000)) {
          return {
            interval: new Decimal(1000),
            time: this._time,
            elapsedTime: this._elapsedTime,
            realTime: value,
            tick: this._tick,
            debug: this.options.debug,
            offset: interval.sub(1000)
          };
        } else {
          return {
            interval,
            time: this._time,
            elapsedTime: this._elapsedTime,
            realTime: value,
            tick: this._tick,
            debug: this.options.debug,
            offset: new Decimal(0)
          };
        }
      })
    );

    this.loop$ = this.running$.pipe(
      tap(x => this.options.debug && console.debug('running$', x)),
      tap(x => log.add(x ? 'System running' : 'System paused')),
      switchMap(active => iif(() => active, this.loopSource$, EMPTY)),
      publish<Loop>()
    ) as ConnectableObservable<Loop>;

    this.loop$.subscribe({
      next: ({ time, interval, elapsedTime }) => {
        if (this.options.debug) {
          if (this._tick.gt(0) && this._tick.toNumber() % 1000 === 0) {
            console.info(`game/loop tick! (${interval}ms) ${this._tick} : ${time}`);
          }
        }

        this._elapsedTime = this._elapsedTime.add(interval);
        this._time = this._time.add(interval);
        this._tick = this._tick.add(1);

        if (this.stepOnce) {
          this.stepOnce = false;
          this.pause();
        }

        if (this.savedAt === null || elapsedTime.gte(this.savedAt.add(this.options.saveInterval))) {
          this.savedAt = elapsedTime;
          this.serialization.saveState(elapsedTime);
        }
      },
      error: err => console.error('game/loop error!', err),
      complete: () => console.info('game/loop complete!')
    });

    this.loop$.connect();
    this.running$.next(true);
  }

  get tick(): Decimal {
    return this._tick;
  }
  get time(): Decimal {
    return this._time;
  }
  get projectedTime(): Decimal {
    return this._elapsedTime;
  }
  get start(): Decimal {
    return this._start;
  }
  public pause(): void {
    this.running$.next(false);
  }
  public play(): void {
    this.running$.next(true);
  }
  public step(): void {
    this.stepOnce = true;
    this.play();
  }
}
