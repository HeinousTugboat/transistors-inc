import { TestBed } from '@angular/core/testing';

import { BonusTicksService } from './bonus-ticks.service';

describe('BonusTickService', () => {
  let service: BonusTicksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BonusTicksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
