import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';

import { OptionsService } from './options.service';

@Injectable({
  providedIn: 'root'
})
export class CheatGuard implements CanActivate {
  constructor(private options: OptionsService) { }

  public canActivate(
    next: ActivatedRouteSnapshot,
  ): boolean {

    if (next.queryParamMap.has('cheat') || next.queryParamMap.has('cheats') || next.queryParamMap.has('admin')) {
      this.options.toggleCheatMode();
    }

    return true;
  }

}
