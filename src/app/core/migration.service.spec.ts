import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';

import { MigrationService } from './migration.service';

describe('MigrationService', () => {
  let service: MigrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MigrationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('fromVersion', () => {
    it('returns null when state is null', () => {
      expect(service.fromVersion(null)).toBeNull();
    });

    it('returns null when state is undefined', () => {
      expect(service.fromVersion(undefined)).toBeNull();
    });

    it('returns null when state does not have a version', () => {
      expect(service.fromVersion({
        upgradeIds: ['credit-multiply_replication-1'],
      })).toBeNull();
    });

    describe('v0.0.1-alpha.3', () => {
      it('returns the expected state', () => {
        expect(service.fromVersion({
          version: '0.0.1-alpha.3',
          upgradeIds: ['credit-multiply_replication-1', 'credit-multiply_cap-1'],
        })).toEqual(jasmine.objectContaining({
          version: environment.version,
          upgradeIds: ['credit-multiply_replication-1', 'credit-multiply_cap-1'],
          chip: '0',
          credit: '0',
          lastSavedAt: jasmine.any(String),
          gameTime: '0',
        }));
      });
    });

    describe('v0.0.1-alpha.4', () => {
      it('returns the expected state', () => {
        expect(service.fromVersion({
          version: '0.0.1-alpha.4',
          upgradeIds: [],
        })).toEqual(jasmine.objectContaining({
          version: environment.version,
          upgradeIds: [],
          chip: '0',
          credit: '0',
          lastSavedAt: jasmine.any(String),
          gameTime: '0',
        }));
      });
    });
  });
});
