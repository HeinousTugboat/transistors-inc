import { TestBed } from '@angular/core/testing';
import { BaseGameState, GameState, Serializable } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';
import { environment } from 'src/environments/environment';

import { initialGameState } from './data/initial-game-state.data';
import { MigrationService } from './migration.service';
import { BROWSER_STORAGE, SerializationService } from './serialization.service';

describe('SerializationService', () => {
  let storageSpy: jasmine.SpyObj<Storage>;
  let migrationSpy: MigrationService;
  let service: SerializationService;

  beforeEach(() => {
    storageSpy = jasmine.createSpyObj<Storage>('Storage', ['setItem', 'getItem']);
    migrationSpy = jasmine.createSpyObj<MigrationService>('MigrationService', ['fromVersion']);

    (migrationSpy as jasmine.SpyObj<MigrationService>).fromVersion.and.callFake((args: GameState) => args);

    TestBed.configureTestingModule({
      providers: [
        { provide: BROWSER_STORAGE, useValue: storageSpy },
        { provide: MigrationService, useValue: migrationSpy }
      ]
    });

    service = TestBed.inject(SerializationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('register', () => {
    it('should accept any object with a partial state key', () => {
      const serializable: Serializable<BaseGameState> = {
        state: { ...initialGameState }
      };

      service.register(serializable);

      // tslint:disable-next-line: no-string-literal
      expect(service['registeredServices']).toContain(serializable);
    });
  });

  describe('saveState', () => {
    let serializable: Serializable<BaseGameState>;
    beforeEach(() => {
      serializable = {
        state: { ...initialGameState }
      };

      service.register(serializable);
    });

    it('should save the state to local storage', () => {
      service.saveState(new Decimal(0));
      expect(storageSpy.setItem).toHaveBeenCalledWith(environment.storageKey, JSON.stringify(serializable.state));
    });
  });

  describe('loadState', () => {
    const serializable: Serializable<BaseGameState> = {
      state: { ...initialGameState }
    };
    beforeEach(() => {
      service.register(serializable);
      serializable.state = {
        lastSavedAt: '0',
        gameTime: '0',
        version: 'test version'
      };

    });

    it('should load the saved state, overriding whatever is currently set', () => {
      storageSpy.getItem.and.returnValue(JSON.stringify(serializable.state));
      serializable.state.version = 'test version two';

      service.loadState();

      expect(serializable.state.version).toBe('test version');
      expect(storageSpy.getItem).toHaveBeenCalledTimes(2);
    });

    it('should work with a string...', () => {
      const data = JSON.stringify(serializable.state);
      serializable.state.version = 'test version two';

      service.loadState(data);

      expect(serializable.state.version).toBe('test version');
      expect(storageSpy.getItem).toHaveBeenCalledTimes(1);
    });
  });

  describe('resetState', () => {
    let serializable: Serializable<BaseGameState>;

    beforeEach(() => {
      serializable = {
        state: {
          lastSavedAt: '0',
          gameTime: '0',
          version: 'test'
        }
      };

      service.register(serializable);
    });

    it('should set the state back to initial values', () => {
      let state: string;

      storageSpy.setItem.and.callFake((_: string, data: string) => { state = data; });
      storageSpy.getItem.and.callFake(() => state);
      service.resetState();

      expect(storageSpy.setItem).toHaveBeenCalledWith(environment.storageKey, JSON.stringify(initialGameState));
      expect(storageSpy.getItem).toHaveBeenCalled();
      expect(serializable.state.version).toBe(environment.version);
      expect((serializable.state as GameState).upgradeIds).toBeDefined();
    });
  });
});
