import { Injectable } from '@angular/core';
import { OptionsState, Serializable } from '@transistors-inc/interfaces';

import { initialGameState } from './data/initial-game-state.data';
import { LogService } from './log.service';
import { SerializationService } from './serialization.service';

@Injectable({
  providedIn: 'root'
})
export class OptionsService implements Serializable<OptionsState> {
  public format = initialGameState.options.format;
  public logLength = initialGameState.options.logLength;
  public saveInterval = initialGameState.options.saveInterval;
  public debug = initialGameState.options.debug;
  public digits = 2;
  public cutoff = 1e10;
  public showSocialMedia = true;
  public aiAllocation?: number;
  public researchAllocation?: number;
  public showIdleAllocation = true;

  private cheatState = initialGameState.options.cheatState;

  get cheatMode(): boolean { return this.cheatState; }

  constructor(
    serialization: SerializationService,
    private log: LogService
  ) {
    serialization.register(this);
  }

  public toggleCheatMode(): void {
    this.cheatState = !this.cheatState;
    this.log.add(`Cheat mode ${this.cheatState ? 'enabled' : 'disabled'}`);
  }

  get state(): OptionsState {
    return {
      options: {
        format: this.format,
        logLength: this.logLength,
        saveInterval: this.saveInterval,
        debug: this.debug,
        cheatState: this.cheatState,
        showSocialMedia: this.showSocialMedia,
        showIdleAllocation: this.showIdleAllocation,
      },
      aiAllocation: this.aiAllocation,
      researchAllocation: this.researchAllocation
    };
  }

  set state(value: OptionsState) {
    this.format = value.options?.format ?? this.format;
    this.logLength = value.options?.logLength ?? this.logLength;
    this.saveInterval = value.options?.saveInterval ?? this.saveInterval;
    this.debug = value.options?.debug ?? this.debug;
    this.cheatState = value.options?.cheatState ?? this.cheatState;
    this.showSocialMedia = value.options?.showSocialMedia ?? this.showSocialMedia;
    this.aiAllocation = value.aiAllocation;
    this.researchAllocation = value.researchAllocation;
    this.showIdleAllocation = value.options?.showIdleAllocation ?? true;
  }
}
