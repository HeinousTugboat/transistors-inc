import { Component } from '@angular/core';
import { AiService } from '@transistors-inc/ai';
import { AiModifierNames } from '@transistors-inc/ai/data';
import { OptionsService } from '@transistors-inc/core';
import { ModService } from '@transistors-inc/game/mod.service';
import { AiModifier, AiTier, ModCategory, ModType } from '@transistors-inc/interfaces';
import { assertNever, defined } from '@transistors-inc/utils';
import { Decimal } from '@vendor';

@Component({
  selector: 'app-ai-list',
  templateUrl: './ai-list.component.html',
  styleUrls: ['./ai-list.component.scss']
})
export class AiListComponent {
  public AiModifierNames = AiModifierNames;
  constructor(
    public ais: AiService,
    public options: OptionsService,
    public mod: ModService
  ) { }

  get aiPromoChance(): Decimal {
    return this.mod.get(ModCategory.SYSTEM, ModType.IMPROVE_AI_PROMOTION).add(200).div(10);
  }

  get aiMods(): Map<ModType, Decimal> {
    return this.mod[ModCategory.AI];
  }

  public getModifierName(type: AiModifier): string {
    return AiModifierNames[type];
  }

  public rarityClass(tier: AiTier): string {
    switch (tier) {
      case AiTier.NUM_TIERS:
      case AiTier.UNDERGRAD: return 'undergrad';
      case AiTier.GRAD: return 'grad';
      case AiTier.PROFESSOR: return 'professor';
      case AiTier.NOBEL: return 'nobel';
      case AiTier.BREAKTHROUGH: return 'breakthrough';
      default:
        return assertNever(tier);
    }
  }

  public addHundo(): void {
    for (let i = 0; i < 100; ++i) {
      this.ais.active.add(this.ais.createAI());
    }
    this.mod.recalculate();
  }

  public erase(tier?: AiTier): void {
    for (const ai of this.ais.active) {
      if (!defined(tier) || ai.tier === tier) {
        this.ais.active.delete(ai);
      }
    }

    this.mod.recalculate();
  }

  public eraseUndergrads(): void { this.erase(AiTier.UNDERGRAD); }
  public eraseGrads(): void { this.erase(AiTier.GRAD); }
  public eraseProfs(): void { this.erase(AiTier.PROFESSOR); }
  public eraseNobels(): void { this.erase(AiTier.NOBEL); }
  public eraseBreakthroughs(): void { this.erase(AiTier.BREAKTHROUGH); }
}
