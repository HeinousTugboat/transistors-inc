import { Location } from '@angular/common';
import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { OptionsService } from '@transistors-inc/core';
import { GlassLabel } from '@transistors-inc/interfaces';
import { TutorialService } from '@transistors-inc/ui/tutorial.service';
import { isElement } from '@transistors-inc/utils';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements AfterViewInit, OnDestroy {
  @ViewChild('helpText') public text!: ElementRef;
  @ViewChild('helpList') public nav!: ElementRef;
  public Glass = GlassLabel;
  private intersectionObserver?: IntersectionObserver;
  private elements: Map<HTMLElement, boolean> = new Map();

  constructor(
    public options: OptionsService,
    public tutorial: TutorialService,
    public loc: Location,
    private elRef: ElementRef
  ) { }

  public handleNavClick(event: MouseEvent, el: HTMLElement, fragment: string): void {
    event.preventDefault();
    this.loc.go(`${this.loc.path(false)}#${fragment}`);
    el.scrollIntoView({ behavior: 'smooth' });
  }

  public elVisible(el: HTMLElement): boolean {
    // tslint:disable-next-line: no-non-null-assertion
    if (this.elements.has(el)) { return this.elements.get(el)!; }
    return false;
  }

  public ngAfterViewInit(): void {
    if (!isElement(this.elRef.nativeElement)) { return; }
    if (!isElement(this.nav.nativeElement)) { return; }
    // if (!isElement(this.text.nativeElement)) { return; }

    const options: IntersectionObserverInit = {
      rootMargin: '0px',
      threshold: [0.0, 1.0]
    };

    const callback: IntersectionObserverCallback = (entries) => {
      entries.forEach((entry) => {
        if (!isElement(entry.target)) { return; }

        this.elements.set(entry.target, entry.isIntersecting);
      });
    };

    const observer = new IntersectionObserver(callback, options);

    // tslint:disable-next-line: no-unsafe-any
    for (const el of this.text.nativeElement.children) {
      // tslint:disable-next-line: no-unsafe-any
      observer.observe(el);
    }
  }

  public ngOnDestroy(): void {
    this.intersectionObserver?.disconnect();
  }

}

