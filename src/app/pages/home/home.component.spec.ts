import { Component, Directive, EventEmitter, Input, Output } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GlassLabel, SliderBar } from '@transistors-inc/interfaces';
import { UnitsPipe } from '@transistors-inc/ui/units.pipe';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    @Component({ selector: 'app-log', template: '' })
    class LogComponentStub { }

    @Component({ selector: 'app-diagnostics', template: '' })
    class DiagnosticsComponentStub { }

    @Component({ selector: 'app-slider-set', template: '' })
    class SliderSetStub {
      @Input() public bars: Set<SliderBar> = new Set();
      @Input() public max = 100;
      @Input() public step = 100;
      @Output() public barsChange = new EventEmitter<Set<SliderBar>>();
    }

    @Directive({ selector: '[appGlass]' })
    class GlassDirectiveStub {
      // tslint:disable-next-line: no-input-rename
      @Input('appGlass') public label!: GlassLabel;
    }

    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        GlassDirectiveStub,
        LogComponentStub,
        DiagnosticsComponentStub,
        SliderSetStub,
        UnitsPipe
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
