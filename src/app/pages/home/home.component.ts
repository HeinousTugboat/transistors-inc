import { Component } from '@angular/core';
import { AiTrainerService } from '@transistors-inc/ai';
import { CurrencyService, OptionsService } from '@transistors-inc/core';
import { Currency, GlassLabel, SliderBar } from '@transistors-inc/interfaces';
import { TransistorsService } from '@transistors-inc/transistor';
import { defined } from '@transistors-inc/utils';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  public Glass = GlassLabel;
  private idleAllocationSlider: SliderBar = {
    label: 'Idle',
    value: 0,
    lock: false
  };

  private aiAllocationSlider: SliderBar = {
    label: 'AI',
    value: 0,
    lock: false
  };

  private researchAllocationSlider: SliderBar = {
    label: 'research',
    value: 0,
    lock: false
  };

  public bars: Set<SliderBar> = new Set();
  public barMax = 100;

  constructor(
    public options: OptionsService,
    public transistors: TransistorsService,
    public aiTrainer: AiTrainerService,
    public currency: CurrencyService
  ) {
    if (options.showIdleAllocation) {
      this.bars.add(this.idleAllocationSlider);
    }

    if (this.currency[Currency.CHIP].gt(0) || defined(options.aiAllocation)) {
      this.bars.add(this.aiAllocationSlider);

      if (defined(options.aiAllocation)) {
        this.aiAllocationSlider.value = options.aiAllocation * 100;
      }
    }

    if (defined(options.researchAllocation)) {
      this.researchAllocationSlider.value = options.researchAllocation * 100;
      this.bars.add(this.researchAllocationSlider);
    }

    if (this.bars.has(this.idleAllocationSlider)) {
      this.idleAllocationSlider.value = 100 - this.aiAllocationSlider.value - this.researchAllocationSlider.value;
    }
  }

  public addAiBar(): void {
    this.bars.add(this.aiAllocationSlider);
  }

  public addResearchBar(): void {
    this.bars.add(this.researchAllocationSlider);
  }

  public handleBarsChange(bars: Set<SliderBar>): void {
    if (this.bars.has(this.aiAllocationSlider)) {
      this.options.aiAllocation = this.aiAllocationSlider.value / 100;
    }

    if (this.bars.has(this.researchAllocationSlider)) {
      this.options.researchAllocation = this.researchAllocationSlider.value / 100;
    }
    this.bars = bars;
  }
}
