import { Component } from '@angular/core';
import { OptionsService } from '@transistors-inc/core/options.service';
import { SerializationService } from '@transistors-inc/core/serialization.service';
import { GlassLabel } from '@transistors-inc/interfaces';
import { TutorialService } from '@transistors-inc/ui/tutorial.service';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent {
  public GlassLabel = Object.keys(GlassLabel);
  constructor(
    public options: OptionsService,
    public tutorial: TutorialService,
    private serialization: SerializationService,
  ) { }

  public reset(): void {
    this.serialization.resetState();
  }

  public toggleIdle(): void {
    this.options.showIdleAllocation = !this.options.showIdleAllocation;
  }


  public toggleLabel(label: keyof typeof GlassLabel): void {
    this.tutorial.toggleLabel(GlassLabel[label]);
  }

  public checkLabel(label: keyof typeof GlassLabel): boolean {
    return this.tutorial.check(GlassLabel[label]);
  }
}
