import { Component, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Upgrade, UpgradeModCategory, UpgradeModifier } from '@transistors-inc/interfaces';
import { UnitsPipe } from '@transistors-inc/ui/units.pipe';

import { UpgradesComponent } from './upgrades.component';

describe('UpgradesComponent', () => {
  let component: UpgradesComponent;
  let fixture: ComponentFixture<UpgradesComponent>;

  beforeEach(async(() => {
    @Component({ selector: 'app-upgrade-card', template: '' })
    class UpgradeCardStub {
      @Input() public sourceList!: Upgrade[];
      @Input() public modType!: UpgradeModifier;
      @Input() public modCategory!: UpgradeModCategory;
    }

    TestBed.configureTestingModule({
      declarations: [
        UpgradeCardStub,
        UpgradesComponent,
        UnitsPipe
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
