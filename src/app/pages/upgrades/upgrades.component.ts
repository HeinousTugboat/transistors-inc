import { Component } from '@angular/core';
import { CurrencyService } from '@transistors-inc/core';
import { ModService } from '@transistors-inc/game/mod.service';
import { ModCategory, ModType } from '@transistors-inc/interfaces';
import { TransistorsService } from '@transistors-inc/transistor';
import { UpgradesService } from '@transistors-inc/upgrades';
import { ModTypeNames } from '@transistors-inc/upgrades/data';
import { chipUpgrades, creditUpgrades, cycleUpgrades } from '@transistors-inc/upgrades/data';
import { Decimal } from '@vendor';

@Component({
  selector: 'app-upgrades',
  templateUrl: './upgrades.component.html',
  styleUrls: ['./upgrades.component.scss']
})
export class UpgradesComponent {
  public creditUpgrades = creditUpgrades;
  public cycleUpgrades = cycleUpgrades;
  public chipUpgrades = chipUpgrades;
  public modType = ModType;
  public modCategory = ModCategory;

  constructor(
    public upgrades: UpgradesService,
    public currency: CurrencyService,
    public transistors: TransistorsService,
    public mod: ModService
  ) { }

  get creditMods(): Map<ModType, Decimal> {
    return this.mod[ModCategory.CREDIT];
  }

  get cycleMods(): Map<ModType, Decimal> {
    return this.mod[ModCategory.CYCLE];
  }

  get chipMods(): Map<ModType, Decimal> {
    return this.mod[ModCategory.CHIP];
  }

  public getModifierName(type: ModType): string {
    return ModTypeNames[type];
  }
}
