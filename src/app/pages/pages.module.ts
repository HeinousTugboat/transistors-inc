import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { AiModule } from '@transistors-inc/ai/ai.module';
import { UiModule } from '@transistors-inc/ui/ui.module';
import { UpgradesModule } from '@transistors-inc/upgrades/upgrades.module';
import { MarkdownModule } from 'ngx-markdown';

import { AiListComponent } from './ai-list/ai-list.component';
import { ChangelogComponent } from './changelog/changelog.component';
import { HelpComponent } from './help/help.component';
import { HomeComponent } from './home/home.component';
import { LogbookComponent } from './logbook/logbook.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { OptionsComponent } from './options/options.component';
import { PagesRoutingModule } from './pages-routing.module';
import { UpgradesComponent } from './upgrades/upgrades.component';

@NgModule({
  declarations: [
    AiListComponent,
    ChangelogComponent,
    HelpComponent,
    HomeComponent,
    NotFoundComponent,
    OptionsComponent,
    UpgradesComponent,
    LogbookComponent,
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    UiModule,
    AiModule,
    UpgradesModule,
    MarkdownModule.forRoot({ loader: HttpClient }) as ModuleWithProviders<MarkdownModule>,
  ]
})
export class PagesModule { }
