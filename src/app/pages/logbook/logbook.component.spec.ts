import { Component, Directive, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GlassLabel } from '@transistors-inc/interfaces';

import { LogbookComponent } from './logbook.component';

describe('LogbookComponent', () => {
  let component: LogbookComponent;
  let fixture: ComponentFixture<LogbookComponent>;

  beforeEach(async(() => {
    @Component({ selector: 'app-log', template: '' })
    class LogComponentStub { }

    @Directive({ selector: '[appGlass]' })
    class GlassDirectiveStub {
      // tslint:disable-next-line: no-input-rename
      @Input('appGlass') public label!: GlassLabel;
    }

    TestBed.configureTestingModule({
      declarations: [
        LogComponentStub,
        GlassDirectiveStub,
        LogbookComponent,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogbookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
