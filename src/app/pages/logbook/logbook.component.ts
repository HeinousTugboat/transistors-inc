import { Component } from '@angular/core';
import { GlassLabel } from '@transistors-inc/interfaces';

@Component({
  selector: 'app-logbook',
  templateUrl: './logbook.component.html',
  styleUrls: ['./logbook.component.scss']
})
export class LogbookComponent {
  public Glass = GlassLabel;
}
