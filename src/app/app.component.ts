import { Component, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { OptionsService } from '@transistors-inc/core';
import { KeybindService } from '@transistors-inc/core/keybind.service';
import { LogService } from '@transistors-inc/core/log.service';
import { TickService } from '@transistors-inc/game/tick.service';
import { GlassLabel } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // tslint:disable-next-line: no-unsafe-any
  @HostBinding('class.sans-condensed') get sansCondensed(): boolean { return false; }
  public Glass = GlassLabel;

  constructor(
    private router: Router,
    public options: OptionsService,
    log: LogService,
    keybind: KeybindService,
    tick: TickService,
  ) {
    log.add('System ready');

    const keys = ['u', 'i', 'o', 'p', 'U', 'I', 'O', 'P'];

    keybind.key$.pipe(
      filter((ev: KeyboardEvent) => ev.type === 'keyup' && keys.includes(ev.key))
    ).subscribe(ev => this.keyup(ev));
    tick.run(new Decimal(0));
  }

  public keyup(ev: KeyboardEvent): void {
    switch (ev.key) {
      case 'U': case 'u': this.router.navigateByUrl('upgrades'); break;
      case 'I': case 'i': this.router.navigateByUrl('ais'); break;
      case 'U': case 'o': this.router.navigateByUrl('options'); break;
      case 'P': case 'p': this.router.navigateByUrl(''); break;
    }
  }
}
