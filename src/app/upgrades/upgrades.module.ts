import { NgModule } from '@angular/core';
import { UiModule } from '@transistors-inc/ui/ui.module';

import { UpgradeCardComponent } from './upgrade-card/upgrade-card.component';

@NgModule({
  declarations: [
    UpgradeCardComponent
  ],
  imports: [
    UiModule,
  ],
  exports: [
    UpgradeCardComponent
  ]
})
export class UpgradesModule { }
