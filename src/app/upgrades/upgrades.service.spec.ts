import { TestBed } from '@angular/core/testing';
import { SerializationService } from '@transistors-inc/core';
import { initialGameState } from '@transistors-inc/core/data';
import { Decimal } from '@vendor';

import { cap, replication } from './data/chip-upgrade.data';
import { UpgradesService } from './upgrades.service';

describe('UpgradeService', () => {
  let serializationSpy: jasmine.SpyObj<SerializationService>;
  let service: UpgradesService;

  beforeEach(() => {
    serializationSpy = jasmine.createSpyObj<SerializationService>('SerializationService', ['loadState', 'saveState', 'register']);
    TestBed.configureTestingModule({
      providers: [
        { provide: SerializationService, useValue: serializationSpy }
      ]
    });
  });

  it('should be created', () => {
    service = TestBed.inject(UpgradesService);
    expect(service).toBeTruthy();
    expect(service.purchased.size).toEqual(0);
  });

  describe('saved upgrades', () => {
    it('should rehydrate correctly', () => {
      service = TestBed.inject(UpgradesService);
      service.state = {
        ...initialGameState,
        upgradeIds: ['chip-multiply_replication-1', 'chip-multiply_cap-1'],
      };

      expect(service.purchased.size).toEqual(2);
      expect(service.available.has(replication(new Decimal(2)))).toBeTrue();
      expect(service.available.has(cap(new Decimal(2)))).toBeTrue();
    });
  });
});
