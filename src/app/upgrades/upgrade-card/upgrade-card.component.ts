import { Component, Input } from '@angular/core';
import * as icons from '@fortawesome/free-regular-svg-icons';
import * as solidIcons from '@fortawesome/free-solid-svg-icons';
import { ModService } from '@transistors-inc/game/mod.service';
import { Upgrade, UpgradeModCategory, UpgradeModifier } from '@transistors-inc/interfaces';
import { defined } from '@transistors-inc/utils';

import { ModTypeNames, UpgradeModifierNames } from '../data';
import { UpgradesService } from '../upgrades.service';

@Component({
  selector: 'app-upgrade-card',
  templateUrl: './upgrade-card.component.html',
  styleUrls: ['./upgrade-card.component.scss']
})
export class UpgradeCardComponent {
  @Input() public sourceList!: Upgrade[];
  @Input() public modType!: UpgradeModifier;
  @Input() public modCategory!: UpgradeModCategory;
  public solid = solidIcons;
  public icons = icons;
  public defined = defined;

  constructor(public upgrades: UpgradesService, public mod: ModService) { }

  get name(): string {
    return UpgradeModifierNames[this.modCategory][this.modType];
  }

  get modName(): string {
    return ModTypeNames[this.modType];
  }

  get canAfford(): boolean {
    return defined(this.upgrade) && this.upgrades.canAfford(this.upgrade);
  }

  get upgrade(): Upgrade | undefined {
    const [upgrade] = this.sourceList.filter((u: Upgrade) => this.upgrades.available.has(u));
    return upgrade;
  }

  public handleClick(): void {
    if (!this.canAfford) { return; }
    if (!defined(this.upgrade)) { return; }

    this.upgrades.purchase(this.upgrade);
  }
}
