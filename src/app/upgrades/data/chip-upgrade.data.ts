import { Currency, ModCategory, ModType, PartialGeneratorConfig, Upgrade } from '@transistors-inc/interfaces';
import { buildUpgradeGenerator } from '@transistors-inc/utils';
import { Decimal } from '@vendor';

import { UpgradeModifierNames } from './upgrade.data';

export const replicationUpgrades: Upgrade[] = [];
export const capUpgrades: Upgrade[] = [];
export const capGrowthUpgrades: Upgrade[] = [];

const buildChipGen = (config: PartialGeneratorConfig) => buildUpgradeGenerator({
  ...config,
  category: ModCategory.CHIP,
  currency: Currency.CHIP,
  descriptionText: UpgradeModifierNames[ModCategory.CHIP][config.type],
});

export const replication = buildChipGen({
  source: replicationUpgrades,
  name: (level: Decimal) => `Enhancement Unit #${level}`,
  type: ModType.MULTIPLY_REPLICATION,
  modAmount: new Decimal(0.1),
});

export const cap = buildChipGen({
  source: capUpgrades,
  name: (level: Decimal) => `Containment Cell #${level}`,
  type: ModType.MULTIPLY_CAP,
  modAmount: new Decimal(1),
});

export const capGrowth = buildChipGen({
  source: capGrowthUpgrades,
  name: (level: Decimal) => `Advanced Storage Agent #${level}`,
  type: ModType.MULTIPLY_CAP_GROWTH,
  modAmount: new Decimal(1),
});
