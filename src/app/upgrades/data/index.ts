import * as chipUpgrades from './chip-upgrade.data';
import * as creditUpgrades from './credit-upgrade.data';
import * as cycleUpgrades from './cycle-upgrade.data';

export * from './upgrade.data';
export {
  cycleUpgrades,
  creditUpgrades,
  chipUpgrades
};
