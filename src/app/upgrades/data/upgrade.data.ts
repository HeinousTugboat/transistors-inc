import { ModCategory, ModType } from '@transistors-inc/interfaces';

export const ModTypeNames: { [key in ModType]: string } = {
  [ModType.IMPROVE_AI_PROMOTION]: 'Improve AI Promotion',
  [ModType.DECREASE_AUTOBUY_DELAY]: 'Autobuy Delay',
  [ModType.ELEVATE_CYCLE_GENERATION]: '',
  [ModType.ELEVATE_CREDIT_EXCHANGE]: '',
  [ModType.IMPROVE_CAP_GROWTH]: '',
  [ModType.IMPROVE_CAP]: 'Cap Increase',
  [ModType.IMPROVE_CREDIT_EXCHANGE]: '',
  [ModType.IMPROVE_REPLICATION]: 'Replication Increase',
  [ModType.INCREASE_ROUND_LENGTH]: 'Round Length',
  [ModType.MULTIPLY_CAP_GROWTH]: 'Cap Growth Multiplier',
  [ModType.MULTIPLY_CAP]: 'Cap Multiplier',
  [ModType.MULTIPLY_CYCLE_GENERATION]: '',
  [ModType.MULTIPLY_CREDIT_EXCHANGE]: '',
  [ModType.MULTIPLY_REPLICATION]: 'Replication Multiplier',
};

export const UpgradeModifierNames = {
  [ModCategory.CREDIT]: {
    [ModType.MULTIPLY_REPLICATION]: '**CREDIT UPGRADE ERROR**',
    [ModType.MULTIPLY_CAP]: '**CREDIT UPGRADE ERROR**',
    [ModType.MULTIPLY_CAP_GROWTH]: '**CREDIT UPGRADE ERROR**',
    [ModType.INCREASE_ROUND_LENGTH]: '+1 sec Round Length',
    [ModType.DECREASE_AUTOBUY_DELAY]: 'Reduce Autobuyer Delay'
  },
  [ModCategory.CYCLE]: {
    [ModType.MULTIPLY_REPLICATION]: 'Basic Enhancer Chip',
    [ModType.MULTIPLY_CAP]: 'Containment Expansion',
    [ModType.MULTIPLY_CAP_GROWTH]: 'Storage Agent',
    [ModType.INCREASE_ROUND_LENGTH]: '**CYCLE UPGRADE ERROR**',
    [ModType.DECREASE_AUTOBUY_DELAY]: '**CYCLE UPGRADE ERROR**'
  },
  [ModCategory.CHIP]: {
    [ModType.MULTIPLY_REPLICATION]: 'Enhancement Unit',
    [ModType.MULTIPLY_CAP]: 'Containment Cell',
    [ModType.MULTIPLY_CAP_GROWTH]: 'Advanced Storage Agent',
    [ModType.INCREASE_ROUND_LENGTH]: '**CHIP UPGRADE ERROR**',
    [ModType.DECREASE_AUTOBUY_DELAY]: '**CHIP UPGRADE ERROR**'

  }
};
