
import { Currency, ModCategory, ModType, PartialGeneratorConfig, Upgrade } from '@transistors-inc/interfaces';
import { buildUpgradeGenerator } from '@transistors-inc/utils';
import { Decimal } from '@vendor';

import { UpgradeModifierNames } from './upgrade.data';

export const replicationUpgrades: Upgrade[] = [];
export const capUpgrades: Upgrade[] = [];
export const capGrowthUpgrades: Upgrade[] = [];

const buildCycleGen = (config: PartialGeneratorConfig) => buildUpgradeGenerator({
  ...config,
  category: ModCategory.CYCLE,
  currency: Currency.CYCLE,
  descriptionText: UpgradeModifierNames[ModCategory.CYCLE][config.type],
});

export const replication = buildCycleGen({
  source: replicationUpgrades,
  name: (level: Decimal) => `Basic Enhancer Chip #${level}`,
  type: ModType.MULTIPLY_REPLICATION,
  modAmount: new Decimal(0.3)
});

export const cap = buildCycleGen({
  source: capUpgrades,
  name: (level: Decimal) => `Containment Expansion #${level}`,
  type: ModType.MULTIPLY_CAP,
  modAmount: new Decimal(2)
});

export const capGrowth = buildCycleGen({
  source: capGrowthUpgrades,
  name: (level: Decimal) => `Storage Agent #${level}`,
  type: ModType.MULTIPLY_CAP_GROWTH,
  modAmount: new Decimal(4)
});

