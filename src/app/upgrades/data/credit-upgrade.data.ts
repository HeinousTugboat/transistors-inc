import { MS_PER_SEC } from '@transistors-inc/core';
import { Currency, ModCategory, ModType, Upgrade } from '@transistors-inc/interfaces';
import { buildUpgradeGenerator } from '@transistors-inc/utils';
import { Decimal } from '@vendor';

export const roundLengthUpgrades: Upgrade[] = [];
export const autobuyDelayUpgrades: Upgrade[] = [];

export const roundLength = buildUpgradeGenerator({
  source: roundLengthUpgrades,
  category: ModCategory.CREDIT,
  currency: Currency.CREDIT,
  name: (level: Decimal) => `+1 sec Round Length #${level}`,
  type: ModType.INCREASE_ROUND_LENGTH,
  modAmount: new Decimal(1),
  costAmount: (level: Decimal) => new Decimal(10).pow(level.sub(1)),
  description: (amount: Decimal) => amount.eq(0) ? `15sec Total` : `+${amount.toNumber()}sec Round Increase — ${amount.add(15).toNumber()}sec Total`
});

export const autobuyDelay = buildUpgradeGenerator({
  source: autobuyDelayUpgrades,
  category: ModCategory.CREDIT,
  currency: Currency.CREDIT,
  name: (level: Decimal) => `Autobuyer Delay #${level}`,
  type: ModType.DECREASE_AUTOBUY_DELAY,
  modAmount: new Decimal(1),
  costAmount: (level: Decimal) => new Decimal(50).mul(new Decimal(10).pow(level)),
  description: (amount: Decimal) => amount.eq(0) ? `No Autobuyer Purchased` : `${MS_PER_SEC.div(amount).floor()}ms`,
});
