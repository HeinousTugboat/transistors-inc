import { Injectable } from '@angular/core';
import {
  CurrencyService,
  LogService,
  SerializationService
} from '@transistors-inc/core';
import { ModService } from '@transistors-inc/game/mod.service';
import {
  Currency as C,
  Mod,
  ModCategory,
  ModProvider,
  ModType,
  Serializable,
  Upgrade,
  UpgradeState
} from '@transistors-inc/interfaces';
import { isModType } from '@transistors-inc/utils/mod';
import { Decimal } from '@vendor';

import { chipUpgrades, creditUpgrades, cycleUpgrades } from './data';

@Injectable({
  providedIn: 'root'
})
export class UpgradesService implements Serializable<UpgradeState>, ModProvider {
  public upgrades: Set<Upgrade> = new Set();
  public available: Set<Upgrade> = new Set();
  public purchased: Set<Upgrade> = new Set();

  public upgradesBought: Record<C, number> = {
    [C.CHIP]: 0,
    [C.TRANSISTOR]: 0,
    [C.CYCLE]: 0,
    [C.CREDIT]: 0,
    [C.BONUS_TICK]: 0,
    [C.FLOP]: 0
  };

  public cycleUpgrades: Partial<Record<ModType, Upgrade>> = {
    [ModType.MULTIPLY_CAP]: cycleUpgrades.cap(new Decimal(1)),
    [ModType.MULTIPLY_REPLICATION]: cycleUpgrades.replication(new Decimal(1)),
    [ModType.MULTIPLY_CAP_GROWTH]: cycleUpgrades.capGrowth(new Decimal(1)),
  };

  public creditUpgrades: Partial<Record<ModType, Upgrade>> = {
    [ModType.INCREASE_ROUND_LENGTH]: creditUpgrades.roundLength(new Decimal(1))
  };

  constructor(
    serialization: SerializationService,
    private mod: ModService,
    private currency: CurrencyService,
    private log: LogService,
  ) {
    this.upgrades.add(creditUpgrades.roundLength(new Decimal(1)));
    this.upgrades.add(creditUpgrades.autobuyDelay(new Decimal(1)));
    this.upgrades.add(chipUpgrades.replication(new Decimal(1)));
    this.upgrades.add(chipUpgrades.cap(new Decimal(1)));
    this.upgrades.add(chipUpgrades.capGrowth(new Decimal(1)));

    serialization.register(this);
    this.resetCycles();
    this.updateAvailable();
    mod.register(this);
  }

  public updateAvailable(): void {
    for (const upgrade of this.upgrades) {
      if (this.purchased.has(upgrade) || this.available.has(upgrade)) {
        this.upgrades.delete(upgrade);
        continue;
      }

      if (upgrade.requirements.every(r => this.purchased.has(r))) {
        this.available.add(upgrade);

        if (upgrade.repeating && upgrade.generator !== undefined) {
          const next = upgrade.generator(upgrade.level.add(1));
          if (!this.upgrades.has(next) && !this.available.has(next) && !this.purchased.has(next)) {
            this.upgrades.add(next);
          }
        }
      }
    }
  }

  public canAfford(u: Upgrade): boolean {
    return this.currency.canAfford(u.cost.currency, u.cost.amount);
  }

  public purchase(u: Upgrade): void {
    if (!this.available.has(u)) { return; }
    if (!this.canAfford(u)) { return; }
    if (!u.requirements.every(r => this.purchased.has(r))) {
      const missingReqs = u.requirements.filter(r => this.purchased.has(r)).map(p => p.id);
      const msg = `Attempted to purchase upgrade ${u.id} without all prereqs! ... ${missingReqs}`;
      this.log.error(msg);
      console.error(msg);
      return;
    }

    this.upgradesBought[u.cost.currency]++;
    this.currency.subtract(u.cost.currency, u.cost.amount);
    this.purchased.add(u);
    this.available.delete(u);
    this.updateAvailable();
    this.mod.recalculate();

    if (u.repeating && u.generator !== undefined) {
      const next = u.generator(u.level.add(1));
      if (!this.upgrades.has(next) && !this.available.has(next) && !this.purchased.has(next)) {
        this.upgrades.add(next);
      }

      if (u.cost.currency === C.CYCLE) {
        this.cycleUpgrades[u.mod.type] = u.generator(u.level.add(1));
      }

      if (u.cost.currency === C.CREDIT) {
        this.creditUpgrades[u.mod.type] = u.generator(u.level.add(1));
      }
    }
  }

  public resetCycles(): void {
    this.upgradesBought[C.CYCLE] = 0;
    this.cycleUpgrades = {
      [ModType.MULTIPLY_CAP]: cycleUpgrades.cap(new Decimal(1)),
      [ModType.MULTIPLY_REPLICATION]: cycleUpgrades.replication(new Decimal(1)),
      [ModType.MULTIPLY_CAP_GROWTH]: cycleUpgrades.capGrowth(new Decimal(1)),
    };

    this.upgrades.add(cycleUpgrades.replication(new Decimal(1)));
    this.upgrades.add(cycleUpgrades.cap(new Decimal(1)));
    this.upgrades.add(cycleUpgrades.capGrowth(new Decimal(1)));

    for (const upgrade of this.available) {
      if (upgrade.cost.currency === C.CYCLE) {
        this.upgrades.add(upgrade);
        this.available.delete(upgrade);
      }
    }

    for (const purchase of this.purchased) {
      if (purchase.cost.currency === C.CYCLE) {
        this.upgrades.add(purchase);
        this.purchased.delete(purchase);
      }
    }

    this.updateAvailable();
    this.mod.recalculate();
  }

  get modList(): Set<Mod> {
    const mods: Set<Mod> = new Set();

    const rarityMod: Mod = {
      category: ModCategory.SYSTEM,
      type: ModType.IMPROVE_AI_PROMOTION,
      amount: new Decimal(0)
    };

    for (const upgrade of this.purchased) {
      mods.add(upgrade.mod);

      if (upgrade.cost.currency === C.CHIP) {
        rarityMod.amount = rarityMod.amount.add(2);
      }
    }

    mods.add(rarityMod);
    return mods;
  }

  get state(): UpgradeState {
    return {
      upgradeIds: Array.from(this.purchased).map((u) => u.id),
    };
  }

  set state(value: UpgradeState) {
    for (const upgrade of this.available) { this.upgrades.add(upgrade); }
    for (const upgrade of this.purchased) { this.upgrades.add(upgrade); }
    this.available.clear();
    this.purchased.clear();

    this.upgradesBought = {
      [C.CHIP]: 0,
      [C.TRANSISTOR]: 0,
      [C.CYCLE]: 0,
      [C.CREDIT]: 0,
      [C.BONUS_TICK]: 0,
      [C.FLOP]: 0,
    };

    this.upgrades.add(chipUpgrades.replication(new Decimal(1)));
    this.upgrades.add(chipUpgrades.cap(new Decimal(1)));
    this.upgrades.add(chipUpgrades.capGrowth(new Decimal(1)));

    for (const upgradeId of value.upgradeIds) {
      let purchasedUpgrade: Upgrade | undefined;

      const [currency, type, level] = upgradeId.split('-');

      if (!isModType(type)) { continue; }

      if (currency === C.CHIP) {
        switch (type) {
          case ModType.MULTIPLY_REPLICATION: purchasedUpgrade = chipUpgrades.replication(new Decimal(level)); break;
          case ModType.MULTIPLY_CAP: purchasedUpgrade = chipUpgrades.cap(new Decimal(level)); break;
          case ModType.MULTIPLY_CAP_GROWTH: purchasedUpgrade = chipUpgrades.capGrowth(new Decimal(level)); break;
        }
      }

      if (currency === C.CREDIT) {
        switch (type) {
          case ModType.INCREASE_ROUND_LENGTH: purchasedUpgrade = creditUpgrades.roundLength(new Decimal(level)); break;
          case ModType.DECREASE_AUTOBUY_DELAY: purchasedUpgrade = creditUpgrades.autobuyDelay(new Decimal(level)); break;
        }
      }

      if (currency === C.CYCLE) {
        switch (type) {
          case ModType.MULTIPLY_REPLICATION: purchasedUpgrade = cycleUpgrades.replication(new Decimal(level)); break;
          case ModType.MULTIPLY_CAP: purchasedUpgrade = cycleUpgrades.cap(new Decimal(level)); break;
          case ModType.MULTIPLY_CAP_GROWTH: purchasedUpgrade = cycleUpgrades.capGrowth(new Decimal(level)); break;
        }

        if (purchasedUpgrade !== undefined) {
          this.upgrades.add(purchasedUpgrade);

          if (purchasedUpgrade.repeating && purchasedUpgrade.generator !== undefined) {
            this.upgrades.add(purchasedUpgrade.generator(purchasedUpgrade.level.add(1)));
          }

          continue;
        }
      }

      if (purchasedUpgrade !== undefined) {
        if (this.available.has(purchasedUpgrade)) {
          this.available.delete(purchasedUpgrade);
        }

        if (this.upgrades.has(purchasedUpgrade)) {
          this.upgrades.delete(purchasedUpgrade);
        }

        this.purchased.add(purchasedUpgrade);
        this.upgradesBought[purchasedUpgrade.cost.currency]++;

        if (purchasedUpgrade.repeating && purchasedUpgrade.generator !== undefined) {
          this.upgrades.add(purchasedUpgrade.generator(purchasedUpgrade.level.add(1)));
        }
      } else {
        this.log.error(`Upgrade ${upgradeId} not found!`);
        console.warn(`Upgrade ${upgradeId} not found!`);
      }
    }

    this.updateAvailable();
    this.mod.recalculate();
  }
}
