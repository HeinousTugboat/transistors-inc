import { Component, Directive, Input } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { GlassLabel } from './interfaces';

describe('AppComponent', () => {
  beforeEach(async(() => {
    @Component({ selector: 'app-nav', template: '' })
    class NavComponentStub { }

    @Component({ selector: 'app-game-buttons', template: '' })
    class GameButtonsStub { }

    @Component({ selector: 'app-data', template: '' })
    class DataStub { }

    @Component({ selector: 'app-selling', template: '' })
    class SellingStub { }

    @Directive({ selector: '[appGlass]' })
    class GlassDirectiveStub {
      // tslint:disable-next-line: no-input-rename
      @Input('appGlass') public label!: GlassLabel;
    }

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        NavComponentStub,
        AppComponent,
        GameButtonsStub,
        DataStub,
        SellingStub,
        GlassDirectiveStub
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
