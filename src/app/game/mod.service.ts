import { Injectable } from '@angular/core';
import { MS_PER_SEC } from '@transistors-inc/core';
import { ModCategory, ModProvider, ModType } from '@transistors-inc/interfaces';
import { assertNever } from '@transistors-inc/utils';
import { Decimal } from '@vendor';

const AI: unique symbol = Symbol.for(ModCategory.AI);
const CYCLE: unique symbol = Symbol.for(ModCategory.CYCLE);
const CREDIT: unique symbol = Symbol.for(ModCategory.CREDIT);
const CHIP: unique symbol = Symbol.for(ModCategory.CHIP);
const SYSTEM: unique symbol = Symbol.for(ModCategory.SYSTEM);

@Injectable({
  providedIn: 'root'
})
export class ModService {
  public static readonly BASE_ROUND_LENGTH = new Decimal(15);

  private registeredServices: Set<ModProvider> = new Set();
  private [AI]: Map<ModType, Decimal> = new Map();
  private [CYCLE]: Map<ModType, Decimal> = new Map();
  private [CREDIT]: Map<ModType, Decimal> = new Map();
  private [CHIP]: Map<ModType, Decimal> = new Map();
  private [SYSTEM]: Map<ModType, Decimal> = new Map();

  public get [ModCategory.AI](): Map<ModType, Decimal> { return this[AI]; }
  public get [ModCategory.CYCLE](): Map<ModType, Decimal> { return this[CYCLE]; }
  public get [ModCategory.CREDIT](): Map<ModType, Decimal> { return this[CREDIT]; }
  public get [ModCategory.CHIP](): Map<ModType, Decimal> { return this[CHIP]; }
  public get [ModCategory.SYSTEM](): Map<ModType, Decimal> { return this[SYSTEM]; }

  public get roundLength(): Decimal {
    return this.get(ModCategory.CREDIT, ModType.INCREASE_ROUND_LENGTH)
      .add(ModService.BASE_ROUND_LENGTH)
      .mul(MS_PER_SEC);
  }

  public register(service: ModProvider): void {
    this.registeredServices.add(service);
    this.recalculate();
  }

  private resetModifier(map: Map<ModType, Decimal>, type: ModType): Decimal {
    switch (type) {
      case ModType.IMPROVE_AI_PROMOTION:
      case ModType.DECREASE_AUTOBUY_DELAY:
      case ModType.IMPROVE_CAP_GROWTH:
      case ModType.IMPROVE_CAP:
      case ModType.IMPROVE_CREDIT_EXCHANGE:
      case ModType.IMPROVE_REPLICATION:
      case ModType.INCREASE_ROUND_LENGTH:
        map.set(type, new Decimal(0));
        break;
      case ModType.ELEVATE_CYCLE_GENERATION:
      case ModType.ELEVATE_CREDIT_EXCHANGE:
      case ModType.MULTIPLY_CAP_GROWTH:
      case ModType.MULTIPLY_CAP:
      case ModType.MULTIPLY_CYCLE_GENERATION:
      case ModType.MULTIPLY_CREDIT_EXCHANGE:
      case ModType.MULTIPLY_REPLICATION:
        map.set(type, new Decimal(1));
        break;
      default:
        assertNever(type);
    }

    return map.get(type) as Decimal;
  }

  public get(category: ModCategory, type: ModType): Decimal {
    if (this[category].has(type)) {
      return this[category].get(type) as Decimal;
    }

    return this.resetModifier(this[category], type);
  }

  public recalculate(): void {
    for (const category of Object.values(ModCategory)) {
      for (const [type] of this[category]) {
        this.resetModifier(this[category], type);
      }
    }

    let totalMod: Decimal;

    for (const service of this.registeredServices) {
      for (const mod of service.modList) {
        totalMod = this.get(mod.category, mod.type);
        this[mod.category].set(mod.type, totalMod.add(mod.amount));
      }
    }
  }
}
