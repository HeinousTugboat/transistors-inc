import { TestBed } from '@angular/core/testing';
import { ModCategory, ModProvider, ModType } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';

import { ModService } from './mod.service';

describe('ModService', () => {
  let service: ModService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add the modifier if not previously referenced', () => {
    expect(service.get(ModCategory.AI, ModType.ELEVATE_CYCLE_GENERATION).toNumber()).toBe(1);
  });

  it('should update mods correctly when a new service registers', () => {
    expect(service.get(ModCategory.AI, ModType.ELEVATE_CYCLE_GENERATION).toNumber()).toBe(1);
    expect(service.get(ModCategory.AI, ModType.MULTIPLY_CYCLE_GENERATION).toNumber()).toBe(1);

    const testModProvider: ModProvider = {
      modList: new Set([{
        category: ModCategory.AI,
        amount: new Decimal(10),
        type: ModType.MULTIPLY_CYCLE_GENERATION,
      }])
    };

    service.register(testModProvider);

    expect(service.get(ModCategory.AI, ModType.ELEVATE_CYCLE_GENERATION).toNumber()).toBe(1);
    expect(service.get(ModCategory.AI, ModType.MULTIPLY_CYCLE_GENERATION).toNumber()).toBe(11);
  });

  it('should update mods correctly when recalculate called', () => {
    expect(service.get(ModCategory.AI, ModType.ELEVATE_CYCLE_GENERATION).toNumber()).toBe(1);
    expect(service.get(ModCategory.AI, ModType.MULTIPLY_CYCLE_GENERATION).toNumber()).toBe(1);

    const testModProvider: ModProvider = {
      modList: new Set([{
        amount: new Decimal(10),
        category: ModCategory.AI,
        type: ModType.MULTIPLY_CYCLE_GENERATION,
      }])
    };

    service.register(testModProvider);

    expect(service.get(ModCategory.AI, ModType.ELEVATE_CYCLE_GENERATION).toNumber()).toBe(1);
    expect(service.get(ModCategory.AI, ModType.MULTIPLY_CYCLE_GENERATION).toNumber()).toBe(11);

    testModProvider.modList.add({
      amount: new Decimal(15),
      category: ModCategory.AI,
      type: ModType.MULTIPLY_CYCLE_GENERATION,
    });

    expect(service.get(ModCategory.AI, ModType.ELEVATE_CYCLE_GENERATION).toNumber()).toBe(1);
    expect(service.get(ModCategory.AI, ModType.MULTIPLY_CYCLE_GENERATION).toNumber()).toBe(11);

    service.recalculate();

    expect(service.get(ModCategory.AI, ModType.ELEVATE_CYCLE_GENERATION).toNumber()).toBe(1);
    expect(service.get(ModCategory.AI, ModType.MULTIPLY_CYCLE_GENERATION).toNumber()).toBe(26);
  });
});
