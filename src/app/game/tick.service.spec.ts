import { TestBed } from '@angular/core/testing';
import { CurrencyService, LoopService } from '@transistors-inc/core';
import { Currency as C } from '@transistors-inc/interfaces';
import { TransistorsService } from '@transistors-inc/transistor';
import { Decimal } from '@vendor';
import { Observable } from 'rxjs';

import { TickService } from './tick.service';

describe('TickService', () => {
  class MockTransistorsService {
    public adjR = new Decimal(1);
    public roundRunning = true;
    public roundDuration = new Decimal(0);
    public cycleTotal = new Decimal(0);
    public m = new Decimal(Infinity);
  }

  class MockLoopService {
    public loop$ = new Observable();
  }

  let transistors: MockTransistorsService;
  let service: TickService;
  let currency: CurrencyService;
  let loop: MockLoopService;

  beforeEach(() => {
    loop = new MockLoopService();
    transistors = new MockTransistorsService();

    TestBed.configureTestingModule({
      providers: [{
        provide: LoopService,
        useValue: loop
      }, {
        provide: TransistorsService,
        useValue: transistors
      }]
    });
    service = TestBed.inject(TickService);
    currency = TestBed.inject(CurrencyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('run', () => {
    it('should math right at 50ms steps', () => {
      const step = 50;
      for (let t = 0; t < 15_000; t += step) {
        service.run(new Decimal(step));
      }

      expect(currency[C.TRANSISTOR].toNumber()).toBeCloseTo((2 ** 15 - 1) * 100);
      expect(transistors.cycleTotal.toNumber()).toBe(47265.1972733734);
    });

    it('should math right at 250ms steps', () => {
      const step = 250;
      for (let t = 0; t < 15_000; t += step) {
        service.run(new Decimal(step));
      }

      expect(currency[C.TRANSISTOR].toNumber()).toBeCloseTo((2 ** 15 - 1) * 100);
      expect(transistors.cycleTotal.toNumber()).toBe(47265.559684084);
    });

    it('should math right at 1ms steps', () => {
      const step = 1;
      for (let t = 0; t < 15_000; t += step) {
        service.run(new Decimal(step));
      }

      expect(currency[C.TRANSISTOR].toNumber()).toBeCloseTo((2 ** 15 - 1) * 100);
      expect(transistors.cycleTotal.toNumber()).toBe(47265.1023764927);
    });

    it('should math right with 50ms steps and a varying rep rate', () => {
      const step = 50;
      for (let t = 0; t < 15_000; t += step) {
        if (t <= 2000) { transistors.adjR = new Decimal(1); }
        else if (t <= 4000) { transistors.adjR = new Decimal(2); }
        else if (t <= 6000) { transistors.adjR = new Decimal(3); }
        else if (t <= 8000) { transistors.adjR = new Decimal(2); }
        else { transistors.adjR = new Decimal(1); }

        service.run(new Decimal(step));
      }

      expect(currency[C.TRANSISTOR].toNumber()).toBeCloseTo((2 ** 9 * 3 ** 4 * 4 ** 2 - 1) * 100);
      expect(transistors.cycleTotal.toNumber()).toBe(954333.197586249);
    });
  });
});
