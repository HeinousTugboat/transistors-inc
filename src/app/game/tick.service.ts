import { Injectable } from '@angular/core';
import { AiTrainerService } from '@transistors-inc/ai';
import { BonusTicksService, CurrencyService, LoopService, MS_PER_SEC } from '@transistors-inc/core';
import { Currency as C, Loop, ModCategory, ModType } from '@transistors-inc/interfaces';
import { TransistorsService, TransistorState } from '@transistors-inc/transistor';
import { Decimal } from '@vendor';

import { ModService } from './mod.service';

@Injectable({
  providedIn: 'root'
})
export class TickService {
  private baseTick = new Decimal(10);
  public transistorTotal = new Decimal(1);
  public cycleTotal = new Decimal(0);
  private extraDelta = new Decimal(0);

  constructor(
    private transistors: TransistorsService,
    private currency: CurrencyService,
    private bonusTicks: BonusTicksService,
    private mod: ModService,
    private aiTrainer: AiTrainerService,
    loop: LoopService,
  ) {
    loop.loop$.subscribe(l => this.update(l));
  }

  public update(loop: Loop): void {
    let time = loop.interval.add(this.extraDelta);

    while (time.gte(this.baseTick)) {
      time = time.sub(this.baseTick);
      this.run(this.baseTick);
    }

    if (time.neq(new Decimal(0))) {
      this.extraDelta = time;
    }
  }

  public run(time: Decimal = this.baseTick): void {
    let dt = time;

    if (this.bonusTicks.enabled) {
      let modT = dt.mul(this.bonusTicks.multiplier);

      if (this.currency[C.BONUS_TICK].gt(modT)) {
        this.currency.subtract(C.BONUS_TICK, modT);
      } else {
        modT = this.currency[C.BONUS_TICK];
        this.currency.set(C.BONUS_TICK, new Decimal(0));
        this.bonusTicks.enabled = false;
      }

      dt = dt.add(modT);
    }

    if (this.transistors.roundRunning) { this.processTransistors(dt); }

    this.currency.add(C.FLOP, this.aiTrainer.allocatedChips.mul(dt).div(MS_PER_SEC));

    this.aiTrainer.run(dt);

    this.currency.subtract(C.FLOP, this.currency[C.FLOP].floor());
  }

  private processTransistors(dt: Decimal): void {
    if (this.transistors.currentState === TransistorState.INCREASE_SPEED) {
      dt = dt.mul(4);
    }

    if (this.transistors.roundDuration.add(dt).gt(this.mod.roundLength)) {
      dt = this.mod.roundLength.sub(this.transistors.roundDuration);
    }

    if (this.transistors.currentState === TransistorState.INCREASE_M) {
      this.transistors.baseM = this.transistors.baseM.add(this.transistors.t.mul(dt).div(MS_PER_SEC).mul(100));
    }

    this.transistors.roundDuration = this.transistors.roundDuration.add(dt);

    dt = dt.div(MS_PER_SEC);

    const r = this.transistors.adjR.add(1);
    const transistors = this.currency[C.TRANSISTOR].add(100);
    const transistorGain = transistors.mul(r.pow(dt)).sub(transistors);
    this.currency.add(C.TRANSISTOR, transistorGain, this.transistors.m);

    const cycleGain = this.currency[C.TRANSISTOR].sub(transistors).div(100).mul(dt)
      .div((this.currency[C.TRANSISTOR].div(transistors)).ln())
      .mul(this.mod.get(ModCategory.AI, ModType.MULTIPLY_CYCLE_GENERATION)
        .pow(this.mod.get(ModCategory.AI, ModType.ELEVATE_CYCLE_GENERATION)));

    this.transistors.cycleTotal = this.transistors.cycleTotal.add(cycleGain);
    this.currency.add(C.CYCLE, cycleGain);
  }
}
