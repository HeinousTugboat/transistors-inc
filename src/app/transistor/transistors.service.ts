import { Injectable } from '@angular/core';
import {
  CurrencyService,
  KeybindService,
  LogService,
  LoopService,
  MS_PER_SEC
} from '@transistors-inc/core';
import { ModService } from '@transistors-inc/game/mod.service';
import {
  Currency as C,
  Loop,
  ModCategory,
  ModType,
  Upgrade
} from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';
import { filter } from 'rxjs/operators';

import { UpgradesService } from '../upgrades'; // FIXME: Break this dependency.

export enum TransistorState {
  INCREASE_SPEED = 'Run Fast',
  INCREASE_R = 'Increase R',
  INCREASE_M = 'Increase M',
}

@Injectable({
  providedIn: 'root'
})
export class TransistorsService {
  public seedN = new Decimal(1);
  public baseR = new Decimal(0.05);
  public startM = new Decimal(10 * 100);
  public baseM = this.startM;
  public baseT = new Decimal(1);
  public roundRunning = false;
  public roundNumber = new Decimal(0);
  public roundDuration = new Decimal(0);
  public currentState = TransistorState.INCREASE_SPEED;
  public cycleTotal = new Decimal(0);
  public builtAI = false;

  public lostPartialT = new Decimal(0);
  public lostAdjT = new Decimal(0);
  public gainAdjT = new Decimal(0);

  private buyCycleReplication = false;
  private buyCycleCap = false;
  private buyCycleCapGrowth = false;
  private lastAutoBuyTime = new Decimal(Infinity);

  constructor(
    private mod: ModService,
    private currency: CurrencyService,
    private log: LogService,
    private upgrades: UpgradesService,
    keybind: KeybindService,
    loop: LoopService,
  ) {
    loop.loop$.subscribe((x) => this.update(x));

    const onUpKeys = ['q', 'w', 'e', 's', 'Q', 'W', 'E', 'S'];
    const onBothKeys = ['1', '2', '3'];

    keybind.key$.pipe(
      filter((ev: KeyboardEvent) => onBothKeys.includes(ev.key) || ev.type === 'keyup' && onUpKeys.includes(ev.key))
    ).subscribe(ev => this.handleKey(ev));
  }

  public handleKey(ev: KeyboardEvent): void {
    switch (ev.key) {
      case 'Q': case 'q': this.setState(TransistorState.INCREASE_SPEED); break;
      case 'W': case 'w': this.setState(TransistorState.INCREASE_R); break;
      case 'E': case 'e': this.setState(TransistorState.INCREASE_M); break;
      case 'S': case 's': this.startRound(); break;
      case '1': this.buyCycleReplication = ev.type === 'keydown'; break;
      case '2': this.buyCycleCap = ev.type === 'keydown'; break;
      case '3': this.buyCycleCapGrowth = ev.type === 'keydown'; break;
    }
  }

  get r(): Decimal {
    return this.baseR
      .mul(this.mod.get(ModCategory.AI, ModType.MULTIPLY_REPLICATION))
      .mul(this.mod.get(ModCategory.CHIP, ModType.MULTIPLY_REPLICATION))
      .mul(this.mod.get(ModCategory.CYCLE, ModType.MULTIPLY_REPLICATION));
  }
  get m(): Decimal {
    return this.baseM
      .mul(this.mod.get(ModCategory.AI, ModType.MULTIPLY_CAP))
      .mul(this.mod.get(ModCategory.CHIP, ModType.MULTIPLY_CAP))
      .mul(this.mod.get(ModCategory.CYCLE, ModType.MULTIPLY_CAP));
  }
  get t(): Decimal {
    return this.baseT
      .mul(this.mod.get(ModCategory.AI, ModType.MULTIPLY_CAP_GROWTH))
      .mul(this.mod.get(ModCategory.CHIP, ModType.MULTIPLY_CAP_GROWTH))
      .mul(this.mod.get(ModCategory.CYCLE, ModType.MULTIPLY_CAP_GROWTH));
  }

  get roundUpgradeCost(): Decimal {
    return this.upgrades.creditUpgrades[ModType.INCREASE_ROUND_LENGTH]?.cost.amount ?? new Decimal(0);
  }
  get adjR(): Decimal {
    if (this.currentState === TransistorState.INCREASE_R) {
      return this.r.mul(1.5);
    }

    if (this.currentState === TransistorState.INCREASE_SPEED) {
      return this.r.mul(1);
    }

    return this.r;
  }
  get roundTotal(): Decimal { return Decimal.exp(this.adjR.mul(this.mod.roundLength).div(MS_PER_SEC)).sub(this.seedN); }
  get adjTotal(): Decimal {
    if (this.roundTotal.gt(this.m)) {
      return this.m;
    } else {
      return this.roundTotal;
    }
  }

  get canStart(): boolean { return !this.roundRunning; }
  get canSell(): boolean { return this.canStart && this.currency[C.CHIP].gt(0); }
  get canUpgradeRound(): boolean { return this.canStart && this.currency.canAfford(C.CREDIT, this.roundUpgradeCost); }

  // N_t = N_(t-1) * e^((r - 1) * dT/1000)
  public baseGain(interval: Decimal = MS_PER_SEC): Decimal {
    const seed = this.seedN.add(this.currency[C.TRANSISTOR]);

    return seed.mul(Decimal.exp(this.r.mul(interval).div(MS_PER_SEC))).sub(seed);
  }
  public rawGain(interval: Decimal = MS_PER_SEC): Decimal {
    const seed = this.seedN.add(this.currency[C.TRANSISTOR]);

    return seed.mul(Decimal.exp(this.adjR.mul(interval).div(MS_PER_SEC))).sub(seed);
  }

  public tickGain(interval: Decimal): Decimal {
    let gain = this.rawGain(interval);
    const remaining = this.m.sub(this.currency[C.TRANSISTOR]);

    if (gain.gt(remaining)) {
      gain = remaining;
    }

    return gain;
  }

  public baseCredits(n: Decimal): Decimal {
    return Decimal.floor(n)
      .mul(this.mod.get(ModCategory.AI, ModType.MULTIPLY_CREDIT_EXCHANGE)
        .pow(this.mod.get(ModCategory.AI, ModType.ELEVATE_CREDIT_EXCHANGE)));
  }

  public startRound(): void {
    if (!this.canStart) { return; }
    // TODO: Add proper round interface/history
    this.roundRunning = true;
    this.roundDuration = new Decimal(0);
    this.roundNumber = this.roundNumber.add(1);
    this.upgrades.resetCycles();
    this.currency[C.CYCLE] = new Decimal(0);
    this.cycleTotal = new Decimal(0);
    this.currency[C.TRANSISTOR] = new Decimal(0);
    this.baseM = this.startM;
    this.log.add(`Round #${this.roundNumber} started`);
  }

  public endRound(): void {
    this.roundRunning = false;
    this.lostPartialT = this.lostPartialT.add(this.currency[C.TRANSISTOR]).sub(Decimal.floor(this.currency[C.TRANSISTOR]));
    this.currency[C.TRANSISTOR] = Decimal.floor(this.currency[C.TRANSISTOR]);
    this.currency.add(C.CHIP, this.currency[C.TRANSISTOR].div(100).floor());
    this.roundDuration = this.mod.roundLength;
    this.builtAI = false;
    // tslint:disable-next-line: max-line-length
    this.log.add(`Round #${this.roundNumber} ended`);
  }

  public setState(state: TransistorState): void {
    this.currentState = state;
  }

  public update(loop: Loop): void {
    if (!this.roundRunning) { return; }

    this.checkAutobuy(loop);

    if (this.roundDuration.gte(this.mod.roundLength)) {
      this.endRound();
    }
  }

  public checkAutobuy(loop: Loop): void {
    if (this.buyCycleReplication) {
      this.upgrades.purchase(this.getCycleUpgrade(ModType.MULTIPLY_REPLICATION));
    }

    if (this.buyCycleCap) {
      this.upgrades.purchase(this.getCycleUpgrade(ModType.MULTIPLY_CAP));
    }

    if (this.buyCycleCapGrowth) {
      this.upgrades.purchase(this.getCycleUpgrade(ModType.MULTIPLY_CAP_GROWTH));
    }

    const autobuyDelay = this.mod.get(ModCategory.CREDIT, ModType.DECREASE_AUTOBUY_DELAY);

    if (autobuyDelay.gt(0)) {
      if (this.lastAutoBuyTime.gt(loop.time)) {
        this.lastAutoBuyTime = loop.time;
      }

      if (loop.time.sub(this.lastAutoBuyTime).gte(MS_PER_SEC.div(autobuyDelay))) {
        let bought = false;
        if (this.canAffordCycleUpgrade(ModType.MULTIPLY_REPLICATION)) {
          this.upgrades.purchase(this.getCycleUpgrade(ModType.MULTIPLY_REPLICATION));
          bought = true;
        } else if (this.canAffordCycleUpgrade(ModType.MULTIPLY_CAP)) {
          this.upgrades.purchase(this.getCycleUpgrade(ModType.MULTIPLY_CAP));
          bought = true;
        } else if (this.canAffordCycleUpgrade(ModType.MULTIPLY_CAP_GROWTH)) {
          this.upgrades.purchase(this.getCycleUpgrade(ModType.MULTIPLY_CAP_GROWTH));
          bought = true;
        }

        if (bought) {
          this.lastAutoBuyTime = loop.time;
        }
      }
    }
  }

  public getCycleUpgrade(type: ModType): Upgrade {
    // tslint:disable-next-line: no-non-null-assertion
    return this.upgrades.cycleUpgrades[type]!;
  }

  public canAffordCycleUpgrade(type: ModType): boolean {
    const u = this.getCycleUpgrade(type);
    return this.currency.canAfford(u.cost.currency, u.cost.amount);
  }

  public sell(n: Decimal): void {
    this.log.add(`Sold transistors for ${n} credits`);
    this.currency.add(C.CREDIT, this.baseCredits(n));
    this.currency.subtract(C.CHIP, n);
  }
}
