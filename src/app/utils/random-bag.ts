import { OddsItem } from '@transistors-inc/interfaces';

import { defined } from './defined';

export class RandomBag<T> {
  private events: T[] = [];
  private totalOdds = 0;
  private bagSize: number;

  constructor(
    private range: (n: number) => number,
    private items: OddsItem<T>[],
    bagSize?: number
  ) {
    if (items.length < 1) { throw new RangeError(`RandomBag: No items provided to draw!`); }
    if (defined(bagSize) && bagSize < items.length) { throw new RangeError(`RandomBag: Bag can't hold one of each item`); }

    this.totalOdds = items.reduce((a, b) => a + b.odds, 0);

    if (defined(bagSize) && bagSize < this.totalOdds) { throw new RangeError(`RandomBag: Bag can't hold one item for each item at given odds`); }

    this.bagSize = bagSize ?? this.totalOdds * 3;

    this.refill();
  }

  public pick(): T {
    const p = this.range(this.events.length);
    const choice = this.events[p];
    this.events.splice(p, 1);

    if (this.events.length < this.bagSize / 2) {
      this.refill();
    }

    return choice;
  }

  private refill(): void {
    for (let i = 0; i < this.bagSize; ++i) {
      let addedItem: OddsItem<T> = this.items[0];

      for (const item of this.items) {
        item.counter = (item.counter ?? 0) + item.odds;

        if (item.counter >= (addedItem.counter ?? 0)) {
          addedItem = item;
        }
      }

      addedItem.counter = (addedItem.counter ?? 0) - this.totalOdds;
      this.events.push(addedItem.item);
    }
  }
}
