import { Upgrade, UpgradeGeneratorConfig } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';

import { defined } from './defined';

export function buildUpgradeGenerator({
  source, category, currency, name, type, modAmount, descriptionText = '',
  description = (amount: Decimal) => `${amount.mul(100).toNumber()}% ${descriptionText}`,
  costAmount = (level: Decimal) => new Decimal(10).pow(level),
  repeating = true
}: UpgradeGeneratorConfig): (level: Decimal) => Upgrade {
  function generator(level: Decimal): Upgrade {
    if (defined(source[level.toNumber() - 1])) {
      return source[level.toNumber() - 1];
    }

    const upgrade: Upgrade = {
      id: `${currency}-${type}-${level}`,
      mod: {
        category,
        type,
        amount: modAmount
      },
      cost: {
        currency,
        amount: costAmount(level)
      },
      level,
      name: name(level),
      repeating,
      generator,
      requirements: [],
      description
    };

    if (level.gt(1)) {
      upgrade.requirements.push(generator(level.sub(1)));
    }

    source[level.toNumber() - 1] = upgrade;

    return upgrade;
  }

  return generator;
}
