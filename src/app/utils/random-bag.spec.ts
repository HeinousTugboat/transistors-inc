import { OddsItem } from '@transistors-inc/interfaces';

import { RandomBag } from './random-bag';

describe('RandomBag', () => {
  let bag: RandomBag<string>;
  let n = 0;
  let range = () => n;
  let items: OddsItem<string>[] = [{ item: 'one', odds: 1, counter: 0 }];

  beforeEach(() => {
    range = () => n;
    n = 0;
    items = [{ item: 'one', odds: 1, counter: 0 }];
  });

  it('exists', () => {
    expect(RandomBag).toBeTruthy();
  });

  describe('constructor', () => {
    it('throws an error when not given any odds', () => {
      expect(() => new RandomBag(range, [])).toThrow();
    });

    it('throws an error when given a bag smaller than the input array', () => {
      expect(() => new RandomBag(range, [...items, ...items], 1)).toThrow();
    });

    it('throws an error when given a bag smaller than total probability space', () => {
      items[0].odds = 10;
      expect(() => new RandomBag(range, items, 1)).toThrow();
    });

    it('works!', () => {
      bag = new RandomBag(range, items);
      expect(bag).toBeTruthy();
    });
  });

  describe('pick', () => {
    describe('even odds', () => {
      it('returns the only string passed if one choice given', () => {
        bag = new RandomBag(range, items);
        const choice = bag.pick();
        expect(choice).toBe('one');
      });

      it('returns the last string if passed two equal choices', () => {
        items.push({ item: 'two', odds: 1, counter: 0 });
        bag = new RandomBag(range, items);

        const choice = bag.pick();

        expect(choice).toBe('two');
      });

      it('returns the string with the highest counter if three equal choices given', () => {
        items.push({ item: 'two', odds: 1, counter: -1 });
        items.push({ item: 'three', odds: 1, counter: -2 });
        bag = new RandomBag(range, items);

        const choice = bag.pick();
        expect(choice).toBe('one');
      });
    });

    describe('2-1 odds', () => {
      beforeEach(() => {
        items = [
          { item: 'one', odds: 1, counter: 0 },
          { item: 'two', odds: 2, counter: 0 }
        ];
      });

      it(`produces ['two', 'one', 'two', 'two', 'one', 'two']`, () => {
        bag = new RandomBag(range, items);

        const choices = new Array(6).fill('').map(() => bag.pick());
        expect(choices).toEqual(['two', 'one', 'two', 'two', 'one', 'two']);
      });
    });
  });

  // https://gamedev.stackexchange.com/a/95696
  describe('A-B-C test', () => {
    beforeEach(() => {
      items = [
        { item: 'A', odds: 5, counter: 0 },
        { item: 'B', odds: 4, counter: 0 },
        { item: 'C', odds: 1, counter: 0 }
      ];
    });

    it(`produces ABABCABABAABABCABABAABABCABABA`, () => {
      bag = new RandomBag(range, items);

      const choices = new Array(30).fill('').map(() => bag.pick()).join('');
      expect(choices).toEqual('ABABCABABAABABCABABAABABCABABA');
    });

    it(`produces BABCABABAABABCABABAABABCABABA`, () => {
      n = 1;
      bag = new RandomBag(range, items);

      const choices = new Array(30).fill('').map(() => bag.pick()).join('');
      expect(choices).toEqual('BABCABABAABABCABABAABABCABABAA');
    });
  });

  describe('works with other types', () => {
    it('produces the same item multiple times', () => {
      const itemOne = {
        label: 'itemOne'
      };

      const itemTwo = {
        label: 'itemTwo'
      };

      const complexItems = [
        { item: itemOne, odds: 1, counter: 0 },
        { item: itemTwo, odds: 1, counter: 0 },
      ];

      const complexBag = new RandomBag(range, complexItems, 40);

      const map = new Map([
        [itemOne, 0],
        [itemTwo, 0]
      ]);

      for (let i = 0; i < 20; ++i) {
        const item = complexBag.pick();
        map.set(item, (map.get(item) ?? 0) + 1);
      }



      expect(map.get(itemOne)).toEqual(10);
      expect(map.get(itemTwo)).toEqual(10);
    });
  });
});
