export function defined<T extends any>(o: T | undefined | null): o is T {
  return o !== undefined && o !== null;
}

export function invalid(o: any | undefined | null): o is undefined | null {
  return o === undefined || o === null;
}
