export function assertNever(o: never): never {
  throw new Error(`${o} should never exist`);
}
