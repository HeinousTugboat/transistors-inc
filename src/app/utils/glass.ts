import { GlassLabel } from '@transistors-inc/interfaces';

export function isGlassLabel(type: unknown): type is GlassLabel {
  return Object.values(GlassLabel).includes(type as GlassLabel);
}
