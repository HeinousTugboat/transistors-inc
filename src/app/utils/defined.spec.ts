import { defined, invalid } from './defined';

describe('defined', () => {
  it('returns false for undefined', () => {
    expect(defined(undefined)).toBeFalse();
  });

  it('returns false for null', () => {
    expect(defined(null)).toBeFalse();
  });

  it('returns true for 0', () => {
    expect(defined(0)).toBeTrue();
  });

  it('returns true for an empty string', () => {
    expect(defined('')).toBeTrue();
  });

  it('returns true for an empty object', () => {
    expect(defined({})).toBeTrue();
  });

  it('returns true for an empty array', () => {
    expect(defined([])).toBeTrue();
  });
});

describe('invalid', () => {
  it('returns true for undefined', () => {
    expect(invalid(undefined)).toBeTrue();
  });

  it('returns true for null', () => {
    expect(invalid(null)).toBeTrue();
  });

  it('returns false for 0', () => {
    expect(invalid(0)).toBeFalse();
  });

  it('returns false for an empty string', () => {
    expect(invalid('')).toBeFalse();
  });

  it('returns false for an empty object', () => {
    expect(invalid({})).toBeFalse();
  });

  it('returns false for an empty array', () => {
    expect(invalid([])).toBeFalse();
  });
});
