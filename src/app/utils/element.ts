// Source: https://stackoverflow.com/questions/384286/how-do-you-check-if-a-javascript-object-is-a-dom-object

import { defined } from './defined';

// Returns true if it is a DOM node
export function isNode(o?: unknown): o is Node {
  if (!defined(o)) { return false; }
  if (o instanceof Node) { return true; }

  return typeof o === 'object'
    && typeof (o as Partial<Node>).nodeType === 'number'
    && typeof (o as Partial<Node>).nodeName === 'string';
}

// Returns true if it is a DOM element
export function isElement(o?: unknown): o is HTMLElement {
  if (!defined(o)) { return false; }
  if (o instanceof HTMLElement) { return true; }

  return typeof o === 'object'
    && (o as HTMLElement).nodeType === 1
    && typeof (o as Partial<HTMLElement>).nodeName === 'string';
}
