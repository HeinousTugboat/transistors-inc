import { AiTier } from '@transistors-inc/interfaces';

import { findTier } from './ai';

describe('ai utils', () => {
  describe('findTier', () => {
    describe('f = 0.8', () => {
      const f = 0.80;
      it('80% chance of undergrad', () => {
        expect(findTier(0.00000, f)).toEqual(AiTier.UNDERGRAD);
        expect(findTier(0.79999, f)).toEqual(AiTier.UNDERGRAD);
      });

      it('16% chance of grad', () => {
        expect(findTier(0.80000, f)).toEqual(AiTier.GRAD);
        expect(findTier(0.95999, f)).toEqual(AiTier.GRAD);
      });

      it('3.2% chance of professor', () => {
        expect(findTier(0.96000, f)).toEqual(AiTier.PROFESSOR);
        expect(findTier(0.99199, f)).toEqual(AiTier.PROFESSOR);
      });
      it('0.64% chance of nobel', () => {
        expect(findTier(0.99200, f)).toEqual(AiTier.NOBEL);
        expect(findTier(0.99839, f)).toEqual(AiTier.NOBEL);
      });
      it('0.16% chance of breakthrough', () => {
        expect(findTier(0.99840, f)).toEqual(AiTier.BREAKTHROUGH);
        expect(findTier(0.99999, f)).toEqual(AiTier.BREAKTHROUGH);
      });
    });

    describe('f = 0.4', () => {
      const f = 0.40;
      it('40% chance of undergrad', () => {
        expect(findTier(0.00000, f)).toEqual(AiTier.UNDERGRAD);
        expect(findTier(0.39999, f)).toEqual(AiTier.UNDERGRAD);
      });

      it('24% chance of grad', () => {
        expect(findTier(0.40000, f)).toEqual(AiTier.GRAD);
        expect(findTier(0.63999, f)).toEqual(AiTier.GRAD);
      });

      it('14.4% chance of professor', () => {
        expect(findTier(0.64000, f)).toEqual(AiTier.PROFESSOR);
        expect(findTier(0.78399, f)).toEqual(AiTier.PROFESSOR);
      });
      it('8.641% chance of nobel', () => {
        expect(findTier(0.78400, f)).toEqual(AiTier.NOBEL);
        expect(findTier(0.87040, f)).toEqual(AiTier.NOBEL);
      });
      it('12.959% chance of breakthrough', () => {
        expect(findTier(0.87041, f)).toEqual(AiTier.BREAKTHROUGH);
        expect(findTier(0.99999, f)).toEqual(AiTier.BREAKTHROUGH);
      });
    });

    describe('f = 0.0', () => {
      const f = 0.01;
      it('1% chance of undergrad', () => {
        expect(findTier(0.00000, f)).toEqual(AiTier.UNDERGRAD);
        expect(findTier(0.01000, f)).toEqual(AiTier.UNDERGRAD);
      });

      it('0.989% chance of grad', () => {
        expect(findTier(0.01001, f)).toEqual(AiTier.GRAD);
        expect(findTier(0.01990, f)).toEqual(AiTier.GRAD);
      });

      it('0.979% chance of professor', () => {
        expect(findTier(0.01991, f)).toEqual(AiTier.PROFESSOR);
        expect(findTier(0.02970, f)).toEqual(AiTier.PROFESSOR);
      });
      it('0.969% chance of nobel', () => {
        expect(findTier(0.02971, f)).toEqual(AiTier.NOBEL);
        expect(findTier(0.03940, f)).toEqual(AiTier.NOBEL);
      });
      it('96.058% chance of breakthrough', () => {
        expect(findTier(0.03941, f)).toEqual(AiTier.BREAKTHROUGH);
        expect(findTier(0.99999, f)).toEqual(AiTier.BREAKTHROUGH);
      });
    });
  });
});
