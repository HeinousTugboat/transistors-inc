import { lerp } from './lerp';

describe('lerp', () => {
  describe('from 100 to 200', () => {
    it('returns the correct values', () => {
      const a = 100;
      const b = 200;

      for (let i = 0; i <= 1.0; i += 0.01) {
        expect(lerp(a, b, i)).toBeCloseTo(100 * i + 100);
      }
    });
  });

  describe('from 0 to 1000', () => {
    it('returns 0 at t = 0.0', () => {
      expect(lerp(0, 1000, 0.0)).toBe(0);
    });
    it('returns 1000 at t = 1.0', () => {
      expect(lerp(0, 1000, 1.0)).toBe(1000);
    });
    it('returns -500 at t = -0.5', () => {
      expect(lerp(0, 1000, -0.5)).toBe(-500);
    });
    it('returns 1500 at t = 1.5', () => {
      expect(lerp(0, 1000, 1.5)).toBe(1500);
    });
  });
});
