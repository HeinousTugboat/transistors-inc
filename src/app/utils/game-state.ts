import { GameState } from '@transistors-inc/interfaces';

export function isVersionedGameState(state: unknown): state is Partial<GameState> {
  if (state === null || typeof state !== 'object') { return false; }

  return typeof (state as Partial<GameState>).version === 'string';
}
