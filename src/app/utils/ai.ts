import { AiModTypes, AiTiers } from '@transistors-inc/ai/data/ai.data';
import { AiModifier, AiTier, ComputeTargetedTrainingAi, ModType, TimeTargetedTrainingAi, TrainingAi } from '@transistors-inc/interfaces';

import { defined } from './defined';

export function isAiModifier(type: string): type is AiModifier {
  return AiModTypes.includes(type as ModType);
}

export function isTimeTargeted(ai: TrainingAi): ai is TimeTargetedTrainingAi {
  return defined((ai as TimeTargetedTrainingAi).targetTime);
}

export function isComputeTargeted(ai: TrainingAi): ai is ComputeTargetedTrainingAi {
  return defined((ai as ComputeTargetedTrainingAi).targetCompute);
}

export function findTier(n: number, f: number): AiTier {
  if (n < 0 || n > 1) { throw new RangeError(`choice ${n} is either below 0% or above 100%`); }
  if (f < 0 || f > 1) { throw new RangeError(`range modifier ${f} is either below 0% or above 100%`); }
  for (const tier of AiTiers) {
    if (n < 1 - ((1 - f) ** (tier + 1))) {
      return tier;
    }
  }
  return AiTier.BREAKTHROUGH;
}
