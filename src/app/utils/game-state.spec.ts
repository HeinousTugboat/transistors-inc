import { isVersionedGameState } from './game-state';

describe('isVersionedGameState', () => {
  it('returns false for undefined', () => {
    expect(isVersionedGameState(undefined)).toBeFalse();
  });

  it('returns false for null', () => {
    expect(isVersionedGameState(null)).toBeFalse();
  });

  it('returns false for 0', () => {
    expect(isVersionedGameState(0)).toBeFalse();
  });

  it('returns false for true', () => {
    expect(isVersionedGameState(true)).toBeFalse();
  });

  it('returns false for an empty string', () => {
    expect(isVersionedGameState('')).toBeFalse();
  });

  it('returns false for an empty object', () => {
    expect(isVersionedGameState({})).toBeFalse();
  });

  it('returns false for an empty array', () => {
    expect(isVersionedGameState([])).toBeFalse();
  });

  it('returns false for an object with a non-string version', () => {
    expect(isVersionedGameState({ version: 1 })).toBeFalse();
  });

  it('returns false for an object with undefined version', () => {
    expect(isVersionedGameState({ version: undefined })).toBeFalse();
  });

  it('returns false for an object with null version', () => {
    expect(isVersionedGameState({ version: null })).toBeFalse();
  });

  it('returns false for an object with object version', () => {
    expect(isVersionedGameState({ version: {} })).toBeFalse();
  });

  it('returns false for an object with array version', () => {
    expect(isVersionedGameState({ version: [] })).toBeFalse();
  });

  it('returns false for an object with "true" version', () => {
    expect(isVersionedGameState({ version: true })).toBeFalse();
  });

  it('returns true for an object with a string version', () => {
    expect(isVersionedGameState({ version: 'some string' })).toBeTrue();
  });
});
