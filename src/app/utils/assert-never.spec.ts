import { assertNever } from './assert-never';

describe('assertNever', () => {
  it('throws an error pretty much no matter what you give it...', () => {
    expect(() => assertNever(undefined as never)).toThrow();
    expect(() => assertNever(null as never)).toThrow();
    expect(() => assertNever({} as never)).toThrow();
    expect(() => assertNever([] as never)).toThrow();
    expect(() => assertNever(true as never)).toThrow();
    expect(() => assertNever(false as never)).toThrow();
    expect(() => assertNever(1 as never)).toThrow();
    expect(() => assertNever(0 as never)).toThrow();
    expect(() => assertNever('' as never)).toThrow();
    expect(() => assertNever('test-string' as never)).toThrow();
  });
});
