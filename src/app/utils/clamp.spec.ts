import { clamp } from './clamp';

describe('utils/clamp', () => {
  it('returns n if between min and max', () => {
    expect(clamp(5, 0, 10)).toBe(5);
  });

  it('returns min if n less than min', () => {
    expect(clamp(0, 5, 10)).toBe(5);
  });

  it('returns max if n greater than max', () => {
    expect(clamp(10, 0, 5)).toBe(5);
  });

  describe('no min or max passed', () => {
    it('returns n', () => {
      expect(clamp(5)).toBe(5);
    });
  });

  describe('no min passed', () => {
    it('returns n if less than max', () => {
      expect(clamp(5, undefined, 10)).toBe(5);
    });

    it('returns max if n greater than max', () => {
      expect(clamp(10, undefined, 5)).toBe(5);
    });
  });

  describe('no max passed', () => {
    it('returns n if greater than min', () => {
      expect(clamp(10, 5)).toBe(10);
    });

    it('returns min if n less than min', () => {
      expect(clamp(0, 5)).toBe(5);
    });
  });
});
