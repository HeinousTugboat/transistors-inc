import { ModType } from '@transistors-inc/interfaces';

export function isModType(type: unknown): type is ModType {
  return Object.values(ModType).includes(type as ModType);
}
