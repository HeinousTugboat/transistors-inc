import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UnitsPipe } from '@transistors-inc/ui/units.pipe';

import { SliderSetComponent } from './slider-set.component';

describe('SliderSetComponent', () => {
  let component: SliderSetComponent;
  let fixture: ComponentFixture<SliderSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SliderSetComponent,
        UnitsPipe
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
