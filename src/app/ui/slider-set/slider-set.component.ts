import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SliderBar } from '@transistors-inc/interfaces';
import { clamp, defined } from '@transistors-inc/utils';

@Component({
  selector: 'app-slider-set',
  templateUrl: './slider-set.component.html',
  styleUrls: ['./slider-set.component.scss']
})
export class SliderSetComponent {
  @Input() public bars: Set<SliderBar> = new Set();
  @Input() public max = 100;
  @Input() public step = 100;
  @Output() public barsChange = new EventEmitter<Set<SliderBar>>();

  private localBarsBars?: Set<SliderBar>;

  get barsBars(): Set<SliderBar> {
    if (defined(this.localBarsBars)) { return this.localBarsBars; }
    return this.bars;
  }

  public get sum(): number {
    let total = 0;
    for (const bar of this.bars) {
      total += bar.value;
    }
    return total;
  }

  public handleValue(bar: SliderBar, newValue: number): void {
    if (bar.lock) {
      this.barsChange.emit(this.bars);
      return;
    }
    const bars = [...this.bars.values()];
    const unlockedBars = bars.filter(b => !b.lock && b !== bar);
    const lockedBars = bars.filter(b => b.lock && b !== bar);

    const lockedTotal = lockedBars.reduce((a, b) => a + b.value, 0);
    const unlockedTotal = unlockedBars.reduce((a, b) => a + b.value, 0);

    const prevValue = bar.value;
    const delta = prevValue - newValue;


    if (unlockedBars.length === 0) {
      if (delta < 0 && lockedTotal + newValue < this.max) {
        bar.value = newValue;
      } else if (delta < 0 && lockedTotal + newValue >= this.max) {
        bar.value = this.max - lockedTotal;
      }

      this.barsChange.emit(this.bars);
      return;
    }

    bar.value = newValue;

    if (delta < 0 && unlockedTotal + lockedTotal + newValue < this.max) {
      this.barsChange.emit(this.bars);
      return;
    }

    let extraDelta = 0;
    if (delta !== 0 && unlockedTotal + lockedTotal + prevValue < this.max) {
      extraDelta = clamp(
        this.max - (unlockedTotal + lockedTotal + prevValue),
        -this.max / this.step,
        this.max / this.step
      );
    }


    if (bar.value + lockedTotal > this.max) {
      bar.value = this.max - lockedTotal;
      extraDelta += newValue - (this.max - lockedTotal);
    }

    if (bar.value < 0) {
      extraDelta += bar.value - 0;
      bar.value = 0;
    }

    if (unlockedBars.every(n => n.value === 0)) {
      unlockedBars.forEach(n => n.value = delta / unlockedBars.length);
      this.bars.forEach(b => b.value = clamp(b.value, 0, this.max));
      this.barsChange.emit(this.bars);
      return;
    }

    for (const b of unlockedBars) {
      b.value += b.value * (delta + extraDelta) / unlockedTotal;

      if (b.value + lockedTotal > this.max) {
        extraDelta += b.value - (this.max - lockedTotal);
        b.value = this.max - lockedTotal;
      }

      if (b.value < 0) {
        extraDelta += 0 - b.value;
        b.value = 0;
      }
    }

    this.bars.forEach(b => b.value = clamp(b.value, 0, this.max));
    this.barsChange.emit(this.bars);
  }
}
