import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { DiagnosticsComponent } from './diagnostics/diagnostics.component';
import { GlassDirective } from './glass.directive';
import { LogComponent } from './log/log.component';
import { NavComponent } from './nav/nav.component';
import { CycleComponent } from './panels/cycle/cycle.component';
import { DataComponent } from './panels/data/data.component';
import { GameButtonsComponent } from './panels/game-buttons/game-buttons.component';
import { SellingComponent } from './panels/selling/selling.component';
import { ProgressComponent } from './progress/progress.component';
import { SliderSetComponent } from './slider-set/slider-set.component';
import { SliderComponent } from './slider/slider.component';
import { SocialMediaComponent } from './social-media/social-media.component';
import { UnitsPipe } from './units.pipe';

@NgModule({
  declarations: [
    DiagnosticsComponent,
    LogComponent,
    NavComponent,
    ProgressComponent,
    SliderComponent,
    UnitsPipe,
    GlassDirective,
    SocialMediaComponent,
    SliderSetComponent,
    GameButtonsComponent,
    SellingComponent,
    DataComponent,
    CycleComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    CommonModule,
    FontAwesomeModule,
    GlassDirective,
    DiagnosticsComponent,
    LogComponent,
    NavComponent,
    ProgressComponent,
    SliderComponent,
    SliderSetComponent,
    UnitsPipe,
    SocialMediaComponent,
    GameButtonsComponent,
    SellingComponent,
    DataComponent,
    CycleComponent,
  ]
})
export class UiModule { }
