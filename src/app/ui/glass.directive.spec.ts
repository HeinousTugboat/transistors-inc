import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { GlassLabel } from '@transistors-inc/interfaces';

import { GlassDirective } from './glass.directive';

// https://angular.io/guide/testing-attribute-directives

@Component({
  template: `
  <div>No glass</div>
  <!-- <div appGlass>Missing label</div> -->
  <div [appGlass]="${GlassLabel.LOG}">Log Label</div>
  <div [appGlass]="${GlassLabel.NONE}">No Label</div>`
})
class TestComponent { }

describe('GlassDirective', () => {
  let fixture: ComponentFixture<TestComponent>;
  let des: DebugElement[];
  let bareEl: DebugElement;

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [GlassDirective, TestComponent]
    }).createComponent(TestComponent);

    fixture.detectChanges(); // initial binding

    des = fixture.debugElement.queryAll(By.directive(GlassDirective));
    bareEl = fixture.debugElement.query(By.css('*:not(appGlass)'));
  });

  it('should have three glass elements', () => {
    expect(des.length).toBe(2);
  });

  it('bare element should not have a customProperty', () => {
    // tslint:disable-next-line: no-string-literal
    expect(bareEl.properties['customProperty']).toBeUndefined();
  });
});
