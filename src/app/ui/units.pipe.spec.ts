import { TestBed } from '@angular/core/testing';
import { OptionsService } from '@transistors-inc/core/options.service';

import { UnitsPipe } from './units.pipe';

describe('UnitsPipe', () => {
  let pipe: UnitsPipe;
  let options: OptionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OptionsService]
    });
    options = TestBed.inject(OptionsService);
    pipe = new UnitsPipe(options);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
});
