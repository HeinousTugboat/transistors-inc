import { Component } from '@angular/core';
import { CurrencyService, MS_PER_SEC } from '@transistors-inc/core';
import { Currency as C, GlassLabel } from '@transistors-inc/interfaces';
import { TransistorsService } from '@transistors-inc/transistor';


@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent {
  public MS_PER_SEC = MS_PER_SEC;
  public Glass = GlassLabel;
  public CHIP = C.CHIP;
  public TRANSISTOR = C.TRANSISTOR;
  public CYCLE = C.CYCLE;
  public CREDIT = C.CREDIT;
  public BONUS_TICK = C.BONUS_TICK;

  constructor(
    public currency: CurrencyService,
    public transistors: TransistorsService
  ) { }
}
