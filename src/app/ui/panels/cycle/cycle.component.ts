import { Component } from '@angular/core';
import { CurrencyService } from '@transistors-inc/core';
import { Currency as C, GlassLabel, ModType } from '@transistors-inc/interfaces';
import { TransistorsService } from '@transistors-inc/transistor';
import { UpgradesService } from '@transistors-inc/upgrades';

@Component({
  selector: 'app-cycle',
  templateUrl: './cycle.component.html',
  styleUrls: ['./cycle.component.scss']
})
export class CycleComponent {
  public CYCLE = C.CYCLE;
  public REPLICATION = ModType.MULTIPLY_REPLICATION;
  public CAP = ModType.MULTIPLY_CAP;
  public CAP_GROWTH = ModType.MULTIPLY_CAP_GROWTH;
  public Glass = GlassLabel;

  constructor(
    public transistors: TransistorsService,
    public currency: CurrencyService,
    public upgrades: UpgradesService,
  ) { }
}
