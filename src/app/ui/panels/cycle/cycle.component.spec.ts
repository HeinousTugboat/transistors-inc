import { Directive, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GlassLabel } from '@transistors-inc/interfaces';
import { UnitsPipe } from '@transistors-inc/ui/units.pipe';

import { CycleComponent } from './cycle.component';

describe('CycleComponent', () => {
  let component: CycleComponent;
  let fixture: ComponentFixture<CycleComponent>;

  beforeEach(async(() => {
    @Directive({ selector: '[appGlass]' })
    class GlassDirectiveStub {
      // tslint:disable-next-line: no-input-rename
      @Input('appGlass') public label!: GlassLabel;
    }

    TestBed.configureTestingModule({
      declarations: [
        CycleComponent,
        UnitsPipe,
        GlassDirectiveStub
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
