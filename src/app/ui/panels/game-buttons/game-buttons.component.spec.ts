import { Directive, Input } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GlassLabel } from '@transistors-inc/interfaces';

import { GameButtonsComponent } from './game-buttons.component';

describe('GameButtonsComponent', () => {
  let component: GameButtonsComponent;
  let fixture: ComponentFixture<GameButtonsComponent>;

  beforeEach(async(() => {
    @Directive({ selector: '[appGlass]' })
    class GlassDirectiveStub {
      // tslint:disable-next-line: no-input-rename
      @Input('appGlass') public label!: GlassLabel;
    }

    TestBed.configureTestingModule({
      declarations: [
        GameButtonsComponent,
        GlassDirectiveStub
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
