import { Component } from '@angular/core';
import { AiTrainerService } from '@transistors-inc/ai';
import { BonusTicksService, CurrencyService, MS_PER_SEC } from '@transistors-inc/core';
import { ModService } from '@transistors-inc/game/mod.service';
import { Currency as C, GlassLabel, ProgressBarType } from '@transistors-inc/interfaces';
import { TransistorsService, TransistorState } from '@transistors-inc/transistor';
import { defined } from '@transistors-inc/utils';
import { isComputeTargeted, isTimeTargeted } from '@transistors-inc/utils/ai';
import { Decimal } from '@vendor';

@Component({
  selector: 'app-game-buttons',
  templateUrl: './game-buttons.component.html',
  styleUrls: ['./game-buttons.component.scss']
})
export class GameButtonsComponent {
  public MS_PER_SEC = MS_PER_SEC;
  public Glass = GlassLabel;
  public roundTimerBar = ProgressBarType.ROUND_TIMER;
  public TransistorState = TransistorState;
  public BONUS_TICK = C.BONUS_TICK;

  get aiTraining(): boolean { return defined(this.aiTrainer.inProgress); }
  get aiTarget(): Decimal {
    if (!defined(this.aiTrainer.inProgress)) {
      return new Decimal(-1);
    }

    if (isTimeTargeted(this.aiTrainer.inProgress)) {
      return this.aiTrainer.inProgress.targetTime.div(MS_PER_SEC);
    }

    if (isComputeTargeted(this.aiTrainer.inProgress)) {
      return this.aiTrainer.inProgress.targetCompute.div(MS_PER_SEC);
    }

    return new Decimal(-1);
  }

  get aiProgress(): Decimal {
    if (!defined(this.aiTrainer.inProgress)) {
      return new Decimal(-1);
    }

    if (isTimeTargeted(this.aiTrainer.inProgress)) {
      return this.aiTrainer.inProgress.currentTime.div(this.MS_PER_SEC);
    }

    if (isComputeTargeted(this.aiTrainer.inProgress)) {
      return this.aiTrainer.inProgress.currentCompute.div(this.MS_PER_SEC);
    }

    return new Decimal(-1);
  }

  public trainAi(): void {
    this.aiTrainer.train(new Decimal(10_000));
  }

  constructor(
    public transistors: TransistorsService,
    public aiTrainer: AiTrainerService,
    public currency: CurrencyService,
    public mod: ModService,
    public bonusTicks: BonusTicksService
  ) { }
}
