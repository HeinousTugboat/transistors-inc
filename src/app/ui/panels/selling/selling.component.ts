import { Component } from '@angular/core';
import { CurrencyService } from '@transistors-inc/core';
import { Currency as C } from '@transistors-inc/interfaces';
import { TransistorsService } from '@transistors-inc/transistor';
import { Decimal } from '@vendor';

const DEC_10 = new Decimal(10);
const DEC_1 = new Decimal(1);

@Component({
  selector: 'app-selling',
  templateUrl: './selling.component.html',
  styleUrls: ['./selling.component.scss']
})
export class SellingComponent {
  public one = DEC_1;

  get logTen(): Decimal {
    return Decimal.floor(this.currency[C.CHIP].log10());
  }

  public getSellTier(n: number): Decimal {
    return this.currency[C.CHIP].sub(DEC_10.pow(Decimal.floor(this.currency[C.CHIP].log10()).sub(n)));
  }

  get allTransistors(): Decimal { return Decimal.floor(this.currency[C.CHIP]); }
  public sellOne(): void { this.transistors.sell(new Decimal(1)); }
  public sell(n: Decimal): void { this.transistors.sell(n); }
  public sellAll(): void { this.transistors.sell(this.allTransistors); }

  public creditAmount(n: Decimal = this.currency[C.CHIP]): Decimal {
    return this.transistors.baseCredits(n);
  }

  constructor(
    public transistors: TransistorsService,
    public currency: CurrencyService
  ) { }
}
