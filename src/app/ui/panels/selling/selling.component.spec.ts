import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UnitsPipe } from '@transistors-inc/ui/units.pipe';

import { SellingComponent } from './selling.component';

describe('SellingComponent', () => {
  let component: SellingComponent;
  let fixture: ComponentFixture<SellingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SellingComponent,
        UnitsPipe
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
