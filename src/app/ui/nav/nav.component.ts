import { Component, Inject } from '@angular/core';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { ENV, Environment } from '@transistors-inc/environment';
import { GlassLabel } from '@transistors-inc/interfaces';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  public Glass = GlassLabel;
  public linkIcon = faExternalLinkAlt;
  constructor(@Inject(ENV) public env: Environment) { }
}
