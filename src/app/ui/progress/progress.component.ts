import { Component, Input } from '@angular/core';
import { ProgressBarType } from '@transistors-inc/interfaces';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss']
})
export class ProgressComponent {
  @Input() public type = ProgressBarType.ROUND_TIMER;
  @Input() public value = 0;
  @Input() public min = 0;
  @Input() public max = 0;
  @Input() public low?: number;
  @Input() public high?: number;
  @Input() public optimum?: number;

}
