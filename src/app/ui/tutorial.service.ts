import { Injectable } from '@angular/core';
import { GlassElement, GlassLabel } from '@transistors-inc/interfaces';

@Injectable({
  providedIn: 'root'
})
export class TutorialService {
  private isEnabled = false;
  private list: Set<GlassElement> = new Set();
  private visible: Set<GlassLabel> = new Set([]);

  get active(): boolean { return this.isEnabled; }

  constructor() { }

  private update(): void {
    for (const el of this.list) {
      el.update(this.visible.has(el.label));
    }
  }

  public toggle(): void {
    this.isEnabled = !this.isEnabled;
    this.update();
  }

  public register(el: GlassElement): void {
    this.list.add(el);
  }

  public check(label: GlassLabel): boolean {
    return this.visible.has(label);
  }

  public toggleLabel(label: GlassLabel): void {
    if (this.visible.has(label)) {
      this.visible.delete(label);
    } else {
      this.visible.add(label);
    }
    this.update();
  }
}
