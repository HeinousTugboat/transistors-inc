import { Directive, ElementRef, Input, OnChanges } from '@angular/core';
import { GlassElement, GlassLabel } from '@transistors-inc/interfaces';
import { isElement, isGlassLabel } from '@transistors-inc/utils';

import { TutorialService } from './tutorial.service';

@Directive({
  selector: '[appGlass]'
})
export class GlassDirective implements GlassElement, OnChanges {
  @Input('appGlass') public label!: GlassLabel;
  private visible = true;

  constructor(
    private elRef: ElementRef,
    private tutorial: TutorialService
  ) {
    tutorial.register(this);
    this.update(this.tutorial.check(this.label));
  }

  public ngOnChanges(): void {
    this.update(this.tutorial.check(this.label));
  }

  private updateStyle(): void {
    if (!isElement(this.elRef.nativeElement)) { return; }
    if (this.visible) {
      this.elRef.nativeElement.style.filter = '';
      this.elRef.nativeElement.style.opacity = '';
      this.elRef.nativeElement.style.pointerEvents = '';
      this.elRef.nativeElement.style.userSelect = '';
    } else {
      this.elRef.nativeElement.style.filter = 'blur(3px)';
      this.elRef.nativeElement.style.opacity = '0.3';
      // this.elRef.nativeElement.style.pointerEvents = 'none';
      this.elRef.nativeElement.style.userSelect = 'none';
    }
  }

  public update(active: boolean): void {
    if (isGlassLabel(this.label) && this.label === GlassLabel.NONE) {
      console.error('Missing Glass Label:', this.elRef.nativeElement);
      throw new Error(`Glass Directive requires valid GlassLabel!`);
    }

    if (!isElement(this.elRef.nativeElement)) { return; }
    if (!this.tutorial.active) {
      this.visible = true;
    } else {
      this.visible = active;
    }

    this.updateStyle();
  }
}
