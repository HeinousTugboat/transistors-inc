import { Component } from '@angular/core';
import { AiService } from '@transistors-inc/ai';
import { CurrencyService, MS_PER_SEC, RandomService } from '@transistors-inc/core';
import { ModService } from '@transistors-inc/game/mod.service';
import { Currency as C } from '@transistors-inc/interfaces';
import { TransistorsService } from '@transistors-inc/transistor';
import { Decimal } from '@vendor';

@Component({
  selector: 'app-diagnostics',
  templateUrl: './diagnostics.component.html',
  styleUrls: ['./diagnostics.component.scss']
})
export class DiagnosticsComponent {
  public MS_PER_SEC = MS_PER_SEC;
  public TRANSISTOR = C.TRANSISTOR;
  public CHIP = C.CHIP;
  public timeLeft = this.mod.roundLength.sub(this.transistors.roundDuration ?? 0);
  public randomNumber = 0;

  public randomRangeResults: number[] = [];
  public resultsAverage = 0;

  constructor(
    public transistors: TransistorsService,
    public currency: CurrencyService,
    public mod: ModService,
    private random: RandomService,
    private ai: AiService,
  ) { }

  public gimmeCash(): void {
    this.currency.add(C.CHIP, this.currency[C.CHIP].add(1).pow(10));
  }

  public gimmeBonusTicks(): void {
    this.currency.add(C.BONUS_TICK, new Decimal(600_000));
  }

  public aiNum(): number {
    return this.ai.active.size ?? 0;
  }

  public generateNumber(): void {
    this.randomNumber = this.random.next();
  }

  public generateRange(): void {
    this.randomRangeResults = new Array();
    for (let i = 0; i < 1000; ++i) {
      const n = this.random.range(7);
      this.randomRangeResults[n] = (this.randomRangeResults[n] ?? 0) + 1;
    }

    this.resultsAverage = this.randomRangeResults.reduce((a, b, i) => a + b * (i + 1), 0) / (1000);
  }
}
