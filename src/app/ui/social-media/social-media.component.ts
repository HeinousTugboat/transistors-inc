import { Component } from '@angular/core';
import { faDiscord, faRedditAlien } from '@fortawesome/free-brands-svg-icons';
import { faWindowClose } from '@fortawesome/free-regular-svg-icons';
import { OptionsService } from '@transistors-inc/core';

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styleUrls: ['./social-media.component.scss']
})
export class SocialMediaComponent {
  public discord = faDiscord;
  public reddit = faRedditAlien;
  public closeIcon = faWindowClose;
  constructor(public options: OptionsService) { }

  public close(): void {
    this.options.showSocialMedia = false;
  }
}
