import { PercentPipe } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import { faCircle, faDotCircle, IconDefinition } from '@fortawesome/free-regular-svg-icons';
import { clamp } from '@transistors-inc/utils';
import { Decimal } from '@vendor';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent {
  @Input() public value = 50;
  @Output() public valueChange = new EventEmitter<number>();
  @Input() public min = 0;
  @Input() public max = 100;
  @Input() public low?: number;
  @Input() public high?: number;
  @Input() public optimum?: number;
  @Input() public label = 'Label';
  @Input() public lock = false;
  @Output() public lockChange = new EventEmitter<boolean>();

  private isActive = false;
  private leftEdge = -1;
  private rightEdge = -1;
  private width = -1;
  private percentPipe = new PercentPipe('en');

  get lockIcon(): IconDefinition {
    if (this.lock) {
      return faDotCircle;
    }

    return faCircle;
  }

  get internalValue(): string | null {
    return this.percentPipe.transform(this.value / this.max, '1.2-2');
  }

  public removeMouseMove = () => { };
  public removeMouseUp = () => { };
  public removeTouchMove = () => { };
  public removeTouchEnd = () => { };

  constructor(private event: EventManager) { }

  public handleMouseDown(ev: MouseEvent): void {
    if (this.lock) { return; }
    this.isActive = true;
    const bounds = (ev.target as HTMLElement).getBoundingClientRect();
    this.leftEdge = bounds.left;
    this.rightEdge = bounds.right;
    this.width = this.rightEdge - this.leftEdge;

    this.removeMouseMove = this.event.addGlobalEventListener('document', 'mousemove', this.handleMouseMove.bind(this)) as () => {};
    this.removeMouseUp = this.event.addGlobalEventListener('document', 'mouseup', this.handleMouseUp.bind(this)) as () => {};

    const value = clamp((this.max - this.min) * (ev.clientX - this.leftEdge) / this.width, this.min, this.max);
    if (Number.isNaN(value)) { return; }
    this.valueChange.next(value);
  }

  public handleTouchStart(ev: TouchEvent): void {
    if (this.lock) { return; }
    this.isActive = true;
    const bounds = (ev.target as HTMLElement).getBoundingClientRect();
    this.leftEdge = bounds.left;
    this.rightEdge = bounds.right;
    this.width = this.rightEdge - this.leftEdge;

    this.removeTouchMove = this.event.addGlobalEventListener('document', 'touchmove', this.handleTouchMove.bind(this)) as () => {};
    this.removeTouchEnd = this.event.addGlobalEventListener('document', 'touchend', this.handleTouchEnd.bind(this)) as () => {};

    const value = clamp((this.max - this.min) * (ev.touches[0].clientX - this.leftEdge) / this.width, this.min, this.max);
    if (Number.isNaN(value)) { return; }
    this.valueChange.next(value);

  }

  public handleMouseUp(): void {
    this.isActive = false;
    this.removeMouseMove();
    this.removeMouseUp();
  }

  public handleTouchEnd(): void {
    this.isActive = false;
    this.removeTouchMove();
    this.removeTouchEnd();
  }

  public handleMouseMove(ev: MouseEvent): void {
    if (this.isActive && !this.lock) {
      const value = clamp((this.max - this.min) * (ev.clientX - this.leftEdge) / this.width, this.min, this.max);

      this.valueChange.next(value);
    }
  }

  public handleTouchMove(ev: TouchEvent): void {
    if (this.isActive && !this.lock) {
      const value = clamp((this.max - this.min) * (ev.touches[0].clientX - this.leftEdge) / this.width, this.min, this.max);

      this.valueChange.next(value);
    }
  }

  public handleInputValue(value: string): void {
    if (!this.lock) {
      const parsedValue = new Decimal(value).clamp(this.min, this.max);

      this.valueChange.next(parsedValue.toNumber());
    }
  }
}
