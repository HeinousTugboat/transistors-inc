import {
  AiModifier,
  AiModifierData,
  AiModifierRarity,
  AiTier,
  ModType,
  OddsItem
} from '@transistors-inc/interfaces';

export const AiModTypes: ModType[] = [
  // Normal modifiers
  ModType.MULTIPLY_CAP,
  ModType.MULTIPLY_CAP_GROWTH,
  ModType.MULTIPLY_CYCLE_GENERATION,
  ModType.MULTIPLY_CREDIT_EXCHANGE,

  // Special modifiers
  ModType.MULTIPLY_REPLICATION,
  ModType.ELEVATE_CYCLE_GENERATION,
  ModType.ELEVATE_CREDIT_EXCHANGE,
];

export const AiTiers: AiTier[] = [
  AiTier.UNDERGRAD,
  AiTier.GRAD,
  AiTier.PROFESSOR,
  AiTier.NOBEL,
  AiTier.BREAKTHROUGH,
];

export const AiTierNames: { [key in AiTier]: string } = {
  [AiTier.UNDERGRAD]: 'Undergrad’s',
  [AiTier.GRAD]: 'Grad’s',
  [AiTier.PROFESSOR]: 'Professor’s',
  [AiTier.NOBEL]: 'Nobel’s',
  [AiTier.BREAKTHROUGH]: 'Breakthrough',
  [AiTier.NUM_TIERS]: '',
};


export const AiModifierRanges: { [key in AiModifier]: AiModifierData } = {
  // Normal modifiers
  [ModType.MULTIPLY_CAP]: { min: 2, spread: 2 },
  [ModType.MULTIPLY_CAP_GROWTH]: { min: 3, spread: 6 },
  [ModType.MULTIPLY_CYCLE_GENERATION]: { min: 5, spread: 5 },
  [ModType.MULTIPLY_CREDIT_EXCHANGE]: { min: 5, spread: 5 },

  // Special modifiers
  [ModType.MULTIPLY_REPLICATION]: { min: 1, spread: 1 },
  [ModType.ELEVATE_CYCLE_GENERATION]: { min: 3, spread: 2 },
  [ModType.ELEVATE_CREDIT_EXCHANGE]: { min: 3, spread: 2 },
};

export const AiModifierNames: { [key in AiModifier]: string } = {
  // Normal modifiers
  [ModType.MULTIPLY_CAP]: 'Multiply Cap',
  [ModType.MULTIPLY_CAP_GROWTH]: 'Multiply Cap Growth',
  [ModType.MULTIPLY_CYCLE_GENERATION]: 'Multiply Cycle Generation',
  [ModType.MULTIPLY_CREDIT_EXCHANGE]: 'Multiply Credit Exchange',

  // Special modifiers
  [ModType.MULTIPLY_REPLICATION]: 'Multiply Transistor Replication',
  [ModType.ELEVATE_CYCLE_GENERATION]: 'Elevate Cycle Generation',
  [ModType.ELEVATE_CREDIT_EXCHANGE]: 'Elevate Credit Exchange'
};

export const AiModifierTypeRarities: Record<AiModifierRarity, AiModifier[]> = {
  [AiModifierRarity.NORMAL]: [
    ModType.MULTIPLY_CAP,
    ModType.MULTIPLY_CAP_GROWTH,
    ModType.MULTIPLY_CYCLE_GENERATION,
    ModType.MULTIPLY_CREDIT_EXCHANGE
  ],
  [AiModifierRarity.SPECIAL]: [
    ModType.MULTIPLY_REPLICATION,
    ModType.ELEVATE_CYCLE_GENERATION,
    ModType.ELEVATE_CREDIT_EXCHANGE
  ]
};

export const normalAiModOdds: OddsItem<AiModifier>[] = [
  { item: ModType.MULTIPLY_CAP, odds: 1 },
  { item: ModType.MULTIPLY_CAP_GROWTH, odds: 1 },
  { item: ModType.MULTIPLY_CYCLE_GENERATION, odds: 1 },
  { item: ModType.MULTIPLY_CREDIT_EXCHANGE, odds: 1 }
];

export const rareAiModOdds: OddsItem<AiModifier>[] = [
  { item: ModType.MULTIPLY_REPLICATION, odds: 1 },
  { item: ModType.ELEVATE_CYCLE_GENERATION, odds: 1 },
  { item: ModType.ELEVATE_CREDIT_EXCHANGE, odds: 1 }
];
