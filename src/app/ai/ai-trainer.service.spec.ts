import { TestBed } from '@angular/core/testing';

import { AiTrainerService } from './ai-trainer.service';

describe('AiTrainerService', () => {
  let service: AiTrainerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AiTrainerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
