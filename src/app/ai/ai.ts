import {
  Ai as AiInterface,
  AiJson,
  AiMod,
  AiTier,
  ModCategory
} from '@transistors-inc/interfaces';
import { defined } from '@transistors-inc/utils';
import { isAiModifier } from '@transistors-inc/utils/ai';
import { Decimal } from '@vendor';

import { AiTiers } from './data';

export class Ai implements AiInterface {
  constructor(
    public startTime: Decimal,
    public chips: Decimal,
    public tier: AiTier,
    public lines: AiMod[],
  ) {
    // tslint:disable-next-line: no-bitwise
    this.id = `ai-${this.tier}-${(Math.random() * 0xFFFFFFFF | 0).toString(16).padStart(4, '0')}`;
    this.readyTime = this.startTime.add(10_000);
    this.baseCompute = chips.mul(10_000);
  }
  public id: string;
  public baseCompute: Decimal;
  public trainedCompute = new Decimal(0);

  public readyTime: Decimal;
  public complete = false;

  public static fromJSON(data: AiJson): Ai {
    const lines: (AiMod | null)[] = data.lines.map(line => {
      const [type, amount] = line.split('-');

      if (!isAiModifier(type)) { return null; }

      return { category: ModCategory.AI, type, amount: new Decimal(amount) };
    });

    const ai = new Ai(
      new Decimal(data.startTime),
      new Decimal(data.transistors),
      AiTiers[data.tier],
      lines.filter(defined)
    );

    ai.id = data.id;
    ai.readyTime = new Decimal(data.readyTime);
    ai.complete = data.complete;

    return ai;
  }

  // public train(compute: Decimal): void {
  // }

  public toJSON(): AiJson {
    return {
      id: this.id,
      readyTime: this.readyTime.toString(),
      startTime: this.startTime.toString(),
      complete: this.complete,
      transistors: this.chips.toString(),
      tier: this.tier,
      lines: this.lines.map(line => `${line.type}-${line.amount.toString()}`)
    };
  }
}
