import { NgModule } from '@angular/core';
import { UiModule } from '@transistors-inc/ui/ui.module';

import { AiCardComponent } from './ai-card/ai-card.component';

@NgModule({
  declarations: [
    AiCardComponent
  ],
  imports: [
    UiModule
  ],
  exports: [
    AiCardComponent
  ]
})
export class AiModule { }
