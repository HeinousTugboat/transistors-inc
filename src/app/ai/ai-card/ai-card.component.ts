import { Component, Input } from '@angular/core';
import { AiMod, AiModifier, AiModifierRarity, AiTier } from '@transistors-inc/interfaces';
import { assertNever } from '@transistors-inc/utils';

import { Ai } from '../ai';
import { AiModifierNames, AiModifierTypeRarities, AiTierNames } from '../data';

@Component({
  selector: 'app-ai',
  templateUrl: './ai-card.component.html',
  styleUrls: ['./ai-card.component.scss']
})
export class AiCardComponent {
  @Input() public ai!: Ai;
  @Input() public generating = false;

  public tierName = AiTierNames;

  get rarityClass(): string {
    switch (this.ai.tier) {
      case AiTier.NUM_TIERS:
      case AiTier.UNDERGRAD: return 'undergrad';
      case AiTier.GRAD: return 'grad';
      case AiTier.PROFESSOR: return 'professor';
      case AiTier.NOBEL: return 'nobel';
      case AiTier.BREAKTHROUGH: return 'breakthrough';
      default:
        return assertNever(this.ai.tier);
    }
  }

  public modName(mod: AiModifier): string {
    return AiModifierNames[mod];
  }

  public lineClass(line: AiMod): string {
    if (AiModifierTypeRarities[AiModifierRarity.SPECIAL].includes(line.type)) {
      return 'special';
    }

    return 'normal';
  }
}
