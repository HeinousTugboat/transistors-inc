import { Injectable } from '@angular/core';
import { CurrencyService, KeybindService, LoopService, OptionsService } from '@transistors-inc/core';
import { TrainingAi } from '@transistors-inc/interfaces';
import { Currency as C } from '@transistors-inc/interfaces';
import { isComputeTargeted, isTimeTargeted } from '@transistors-inc/utils/ai';
import { Decimal } from '@vendor';
import { filter } from 'rxjs/operators';

import { Ai } from './ai';
import { AiService } from './ai.service';

@Injectable({
  providedIn: 'root'
})
export class AiTrainerService {
  public active: Set<TrainingAi> = new Set();
  public complete: Set<TrainingAi> = new Set();
  private trainee?: TrainingAi;

  public next = 0;
  public BASE_AI_SLOTS = new Decimal(1);

  constructor(
    private loop: LoopService,
    private currency: CurrencyService,
    private ai: AiService,
    private options: OptionsService,
    keybind: KeybindService,
  ) {

    keybind.key$.pipe(
      filter((ev: KeyboardEvent) => ev.key === 'a' || ev.key === 'A')
    ).subscribe(() => this.train(new Decimal(10_000)));
  }

  get allocatedChips(): Decimal {
    return this.currency[C.CHIP].mul(this.options.aiAllocation ?? new Decimal(0)).floor();
  }

  get canTrain(): boolean {
    return this.allocatedChips.gte(1) && this.BASE_AI_SLOTS.gt(this.active.size);
  }

  get inProgress(): Readonly<TrainingAi | undefined> {
    return this.trainee;
  }

  public finish(trainee: TrainingAi): Ai {
    console.log(`${this.allocatedChips.toFixed(2)} chips allocated for ${trainee.currentTime.toFixed(2)}ms = ${trainee.currentCompute.toFixed(2)} compute`);
    this.active.delete(trainee);
    delete this.trainee;
    this.complete.add(trainee);
    trainee.end = this.loop.time;

    const ai = this.ai.createAI(trainee.currentCompute);
    this.ai.active.add(ai);

    return ai;
  }

  public run(dt: Decimal): void {
    if (this.active.size === 0) { return; }
    if (this.currency[C.CHIP].eq(0)) { return; }
    if (this.allocatedChips.lt(1)) { return; }

    const flops = this.currency[C.FLOP].div(this.active.size).floor();
    const roundRobin = flops.lt(1) && this.currency[C.FLOP].gte(1);
    let current = 0;

    for (const ai of this.active) {
      if (!roundRobin) {
        ai.currentCompute = ai.currentCompute.add(flops);
        this.currency.subtract(C.FLOP, flops);
      }

      if (roundRobin && current === this.next && this.currency[C.FLOP].gte(1)) {
        ai.currentCompute = ai.currentCompute.add(1);
        this.currency.subtract(C.FLOP, new Decimal(1));
        this.next = ++this.next % this.active.size;
      }

      ai.currentTime = ai.currentTime.add(dt);
      ++current;

      if (isComputeTargeted(ai) && ai.currentCompute.gte(ai.targetCompute)
        || isTimeTargeted(ai) && ai.currentTime.gte(ai.targetTime)) {
        this.finish(ai);
      }
    }

    if (this.next >= this.active.size) {
      this.next = 0;
    }
  }

  public train(targetTime?: Decimal, targetCompute?: Decimal): void {
    if (!this.canTrain) { return; }

    const newAi: TrainingAi = {
      start: this.loop.time,
      initialChips: this.currency[C.CHIP],
      currentCompute: new Decimal(0),
      currentTime: new Decimal(0),
      targetCompute,
      targetTime: targetTime ?? new Decimal(10_000)
    };

    this.trainee = newAi;
    this.active.add(newAi);
  }
}
