import { Injectable } from '@angular/core';
import {
  LogService,
  LoopService,
  RandomService,
  SerializationService
} from '@transistors-inc/core';
import { ModService } from '@transistors-inc/game/mod.service';
import {
  AiMod,
  AiModifier,
  AiModifierRarity as ModRarity,
  AiState,
  Loop,
  Mod,
  ModCategory,
  ModProvider,
  ModType,
  Serializable
} from '@transistors-inc/interfaces';
import { RandomBag } from '@transistors-inc/utils';
import { findTier } from '@transistors-inc/utils/ai';
import { Decimal } from '@vendor';

import { Ai } from './ai';
import { AiModifierRanges, normalAiModOdds, rareAiModOdds } from './data/ai.data';

@Injectable({
  providedIn: 'root'
})
export class AiService implements Serializable<AiState>, ModProvider {
  private baseF = new Decimal(800);
  private generating?: Ai;
  private training?: Ai;
  // private trainingStart?: Decimal;
  private trainingStrength?: Decimal;
  public active: Set<Ai> = new Set();

  private normalBag: RandomBag<AiModifier>;
  private rareBag: RandomBag<AiModifier>;

  constructor(
    serialization: SerializationService,
    private loop: LoopService,
    private log: LogService,
    private random: RandomService,
    private mod: ModService,
  ) {
    loop.loop$.subscribe((x) => this.update(x));
    serialization.register(this);
    mod.register(this);

    this.normalBag = new RandomBag((n) => random.range(n), normalAiModOdds);
    this.rareBag = new RandomBag((n) => random.range(n), rareAiModOdds);
  }

  get inProgress(): boolean { return this.generating !== undefined; }
  get generatingAI(): Readonly<Ai | undefined> { return this.generating; }
  get canTrain(): boolean { return this.training === undefined; }
  get running(): boolean { return this.inProgress || !this.canTrain; }
  get f(): number {
    return Math.max(0, this.baseF.sub(this.mod.get(ModCategory.SYSTEM, ModType.IMPROVE_AI_PROMOTION)).div(1000).toNumber());
  }

  private getBag(rarity: ModRarity): RandomBag<AiModifier> {
    if (rarity === ModRarity.SPECIAL) {
      return this.rareBag;
    }

    return this.normalBag;
  }

  public createAI(n: Decimal = new Decimal(10)): Ai {
    const tier = findTier(this.random.next(), this.f);
    const lines: AiMod[] = [];

    for (let line = 0; line < tier + 1; ++line) {
      let rarity = ModRarity.NORMAL;
      const rarityRoll = this.random.next();

      // Line #3 always has a 20% chance of being special.
      // Lines #4 and #5 are always special.
      if (line === 2 && rarityRoll < 0.2 || line >= 3) {
        rarity = ModRarity.SPECIAL;
      }

      let type = this.getBag(rarity).pick();
      while (lines.map(m => m.type).includes(type)) { type = this.getBag(rarity).pick(); }

      const power = n.log10() * (rarity === ModRarity.NORMAL ? tier + 1 : 1);
      const base = AiModifierRanges[type].min * power;
      const mod = this.random.range(Math.floor(AiModifierRanges[type].spread * power));

      lines[line] = {
        category: ModCategory.AI,
        type,
        amount: new Decimal(base + mod).floor().div(100)
      };
      // console.groupCollapsed('line generation:', type);
      // console.log(`base ${type} stats`, 'base', AiModifierRanges[type].min, 'spread', AiModifierRanges[type].spread);
      // console.log('power', power.toFixed(2));
      // console.log('base', base.toFixed(2));
      // console.log('spread', mod.toFixed(2));
      // console.log('result', lines[line].amount.mul(100).toNumber().toFixed(2));
      // console.groupEnd();
    }

    return new Ai(this.loop.time, n, tier, lines);
  }

  public build(n: Decimal): void {
    this.log.add(`Training new AI [f:${this.f}]`);
    this.generating = this.createAI(n);
  }

  public advanceGeneration(time: Decimal): void {
    if (this.generating !== undefined) {
      this.generating.readyTime = this.generating?.readyTime.sub(time);
    }
  }

  public beginTraining(n: Decimal, ai: Ai): void {
    this.training = ai;
    // this.trainingStart = this.loop.time;
    this.trainingStrength = n;
  }

  public endTraining(): void {
    if (this.training !== undefined && this.trainingStrength !== undefined) {
      // this.training.train(this.trainingStrength.mul(this.loop.time.sub(this.trainingStrength)));
    }
  }

  public update(loop: Loop): void {
    if (this.generating !== undefined && loop.time.gte(this.generating?.readyTime)) {
      this.log.add(`AI ${this.generating.id} ready`);
      this.generating.complete = true;
      this.active.add(this.generating);
      delete this.generating;
      this.mod.recalculate();
    }
  }

  get modList(): Set<Mod> {
    const mods: Set<Mod> = new Set();

    for (const ai of this.active) {
      ai.lines.forEach(mods.add, mods);
    }

    return mods;
  }

  get state(): AiState {
    return {
      ai: Array.from(this.active).map(ai => ai.toJSON())
    };
  }

  set state(value: AiState) {
    this.active = new Set(value.ai.map(ai => Ai.fromJSON(ai)));
  }
}
